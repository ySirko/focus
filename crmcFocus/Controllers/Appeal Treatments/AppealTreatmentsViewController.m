//
//  AppealTreatmentsViewController.m
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "AppealTreatmentsViewController.h"
#import "TableViewCell.h"
#import "AddAppealtreatmentViewController.h"
#import "ActionLine.h"

@interface AppealTreatmentsViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation AppealTreatmentsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!_appeal.Descriptionn) {
        NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
        [dic setObject:self.appDelegate.city forKey:@"city"];
        [dic setObject:self.appDelegate.userName  forKey:@"userName"];
        [dic setObject:[NSNumber numberWithInt:_appeal.ID] forKey:@"ticketId"];
        [self.generic showNativeActivityIndicator:self];
        [self.connection connectionToService:[NSString stringWithFormat:@"%@/GetTicketInfoById",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(GetTicketInfoByIdResult:)];
    } else {
        [self SetControllers];
        if(_notification)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:_notification delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
            [alert show];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:self.appDelegate.city forKey:@"city"];
    [dic setObject:self.appDelegate.userName  forKey:@"username"];
    [dic setObject:[NSString stringWithFormat:@"%d",_appeal.ID] forKey:@"TicketId"];
    [dic setObject:[NSNumber numberWithInt:0] forKey:@"ActionLineId"];
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:[NSString stringWithFormat:@"%@/GetActionLinesList",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(GetActionLinesListResult:)];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _listAppeals.count;
}

-(TableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    cell.color=self.color;
    cell.highlightedColor=self.highlightedColor;
    cell.tag=5;
    cell.width=self.width;
    [cell SetActionLineCell: _listAppeals[indexPath.row]];
    return cell;
}

#pragma mark - Private Methods

-(void)GetActionLinesListResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        ActionLine*actionLine=[[ActionLine alloc]init];
        _listAppeals=[actionLine parseActionLineListFromJson:result];
        [_tblAppeals reloadData];
    }
}

-(void)SetControllers
{
    int i=self.lbl.frame.size.height+self.lbl.frame.origin.y;
    _btnAddLine=[[UIButton alloc]initWithFrame:CGRectMake(5, i+5,self.width-10, 25)];
    [_btnAddLine setTitle:@"הוספת שורת טיפול" forState:normal];
    _btnAddLine.layer.cornerRadius=2;
    _btnAddLine.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:14];
    [_btnAddLine setBackgroundColor:self.color];
    [_btnAddLine addTarget:self action:@selector(btnAddLineClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnAddLine];
    
    _lblExsitsLine=[[UILabel alloc]initWithFrame:CGRectMake(5,i+37, self.width-10, 20)];
    _lblExsitsLine.text=@"שורות טיפול קיימות";
    _lblExsitsLine.backgroundColor=[UIColor lightGrayColor];
    _lblExsitsLine.textAlignment=NSTextAlignmentCenter;
    
    [self.view addSubview:_lblExsitsLine];
    
    _tblAppeals=[[UITableView alloc]initWithFrame:CGRectMake(0,i+76,self.width, self.view.frame.size.height-i-70)];
    _tblAppeals.delegate=self;
    _tblAppeals.dataSource=self;
    _tblAppeals.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tblAppeals];
    
    _lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0,i+58, self.width, 4)];
    _lblLine.backgroundColor=self.color;
    [self.view addSubview:_lblLine];
}

- (IBAction)btnAddLineClick:(id)sender
{
    AddAppealtreatmentViewController* view=[[AddAppealtreatmentViewController alloc]init];
    view.appeal=_appeal;
    view.strColor=self.strColor;
    view.color=self.color;
    view.header=[NSString stringWithFormat:@" הוספת שורת טיפול לפניה מס׳ %d",_appeal.ID];
    [self.navigationController pushViewController:view animated:YES];
}

-(void)GetTicketInfoByIdResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        _appeal=[Appeal parseAppealListFromJson:result].firstObject;
        [self SetControllers];
    }
    else
    {
        NSLog(@"ERROR");
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}
@end
