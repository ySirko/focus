//
//  Status.h
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Status : NSObject
@property int Id;
@property(retain,nonatomic)NSString* Text;
-(NSMutableArray*)parseStatusListFromJson:(NSString*) strJson;
@end
