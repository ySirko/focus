//
//  ActionLine.h
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActionLine : NSObject

@property int TicketId;
@property int ActionLineId;
@property(retain,nonatomic)NSDate* OpenDate;
@property int ActionTypeId;
@property(retain,nonatomic)NSString* StatusName;
@property(retain,nonatomic)NSString* EmployerName;
@property(retain,nonatomic)NSString* Text;
@property (strong,nonatomic) NSArray* Pictures;

-(NSMutableArray*)parseActionLineListFromJson:(NSString*) strJson;
-(NSDictionary*)parseActionLineToDict;
-(id)initWith:(int)ticketId ActionTypeId:(int)actionTypeId Text:(NSString *)text Pictures:(NSArray *)pictures;

@end
