//
//  LoginViewController.h
//  crmcFocus
//
//  Created by Lior Ronen on 10/27/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
@interface LoginViewController : SubViewController
@property (strong,nonatomic)UITextField *txtUserName;
@property (strong,nonatomic)UITextField *txtPassword;
@property (strong,nonatomic)UITextField *txtOrganizationCode;
@property (strong, nonatomic)UIButton *btnIsConnected;
@property (strong, nonatomic)UIButton *btnIsEnter;
@property BOOL IsConnected;
@property (strong, nonatomic)UIImageView*imgConnect;
@property (retain, nonatomic) NSString *dt;
@property (strong, nonatomic) NSString *pushSoundName;

@end