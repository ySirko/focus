//
//  Status.m
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "Status.h"

@implementation Status
-(NSMutableArray*)parseStatusListFromJson:(NSString*) strJson
{
    NSError* error;
    NSDictionary* dic=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arr=[[NSMutableArray alloc]init];
    for (NSDictionary* dict in [dic objectForKey:@"Entities"])
        [arr addObject:[self parseStatusFromJson:dict]];
    return  arr;
}
-(Status*)parseStatusFromJson:(NSDictionary*)dic
{
    Status*status=[[Status alloc]init];
    status.Id=(int)[[dic objectForKey:@"Id"] integerValue];
    status.Text= [dic objectForKey:@"Text"];
    return status;
}
@end
