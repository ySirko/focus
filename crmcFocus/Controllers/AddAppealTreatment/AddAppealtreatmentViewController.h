//
//  AddAppealtreatmentViewController.h
//  crmcFocus
//
//  Created by Lior Ronen on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
#import "Appeal.h"
#import "ActionLine.h"
#import "Status.h"
#import "NSData+Base64.h"
#import "imagePreviewViewController.h"

@interface AddAppealtreatmentViewController : SubViewController <UIPickerViewDataSource,UIPickerViewDelegate>

@property (retain, nonatomic) UIImageView *imgView;
@property (retain, nonatomic) UIImageView *imgCamera;
@property (retain, nonatomic) UIButton *btnImag_click;
@property (retain, nonatomic) UIButton *btnAddTreatLine;

@property (retain, nonatomic) Appeal *appeal;

@end
