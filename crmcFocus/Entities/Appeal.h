//
//  Appeal.h
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Appeal : NSObject

@property(retain,nonatomic)NSString*ActionLines;
@property(retain,nonatomic)NSString*Address;
@property BOOL AlreadyRead;
@property(retain,nonatomic)NSString*DepartmentName;
@property(retain,nonatomic)NSString*Descriptionn;
@property(retain,nonatomic)NSDate*ForeCast;
@property int HouseNumber;
@property int ID;
@property(retain,nonatomic)NSDate*OpenDate;
@property(retain,nonatomic)NSDate*SendDate;
@property(retain,nonatomic)NSMutableArray*arrPictures;
@property(retain,nonatomic)NSString*ReporterCellPhoneNumber;
@property(retain,nonatomic)NSString*ReporterFirstName;
@property(retain,nonatomic)NSString*ReporterHomePhoneNumber;
@property(retain,nonatomic)NSString*ReporterLastName;
@property(retain,nonatomic)NSString*SoundFile;
@property int StatusId;
@property(retain,nonatomic)NSString*StatusName;
@property(retain,nonatomic)NSString*Subject;
@property(retain,nonatomic)NSString*TempID;

+ (NSMutableArray*)parseAppealListFromJson:(NSString*)strJson;
- (NSDictionary*)parseAppealToDict;
- (id)initWith:(NSString*)description Address:(NSString*)address HouseNumber:(int)houseNumber Pictures:(NSMutableArray*)pictures;

@end
