//
//  NewAppealViewController.m
//  crmcFocus
//
//  Created by Lior Ronen on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "NewAppealViewController.h"
#import "Street.h"
#import "Appeal.h"
#import "imagePreviewViewController.h"
#import <QuartzCore/QuartzCore.h>
@import AVFoundation;

@interface NewAppealViewController () <UITableViewDataSource,UINavigationControllerDelegate, UITableViewDelegate, UISearchDisplayDelegate,UIAlertViewDelegate, UISearchBarDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>
{
    NSMutableArray*filterStreet;
    UITableView* autoCompleteTableView;
    int tableHeight;
    int streetId;
    NSString* streetName;
    UIAlertView* alertReport;
    int num;
    int YOrigion;
    NSMutableArray*arrimg;
    int numImg;
}

@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *pinkView;

@property (weak, nonatomic) IBOutlet UILabel *lblDateAppeal;
@property (weak, nonatomic) IBOutlet UITextField *txtNumHouse;
@property (weak, nonatomic) IBOutlet UITextField *txtFildAddress;
@property (weak, nonatomic) IBOutlet UITextView *txtViewDescription;
@property (weak, nonatomic) IBOutlet UIButton *leftImageButton;
@property (weak, nonatomic) IBOutlet UIButton *rightImageButton;


@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftImageWidth;

@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightImageWidth;


@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (strong, nonatomic) NSArray *listStreets;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic) BOOL leftButtonState;
@property (nonatomic) BOOL rightButtonState;

@property (nonatomic) BOOL leftImagePicked;
@property (nonatomic) BOOL rightImagePicked;

@property (strong, nonatomic) imagePreviewViewController *IVC;
@property (nonatomic) int senderButton;

@end

@implementation NewAppealViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.image removeFromSuperview];
    [self.imageLogo removeFromSuperview];
    [self.background removeFromSuperview];
    [self.lbl removeFromSuperview];
    [self.lblTitel removeFromSuperview];
    self.pinkView.backgroundColor = [UIColor colorWithRed:76/255.0f
                                                    green:0/255.0f
                                                     blue:121/255.0f
                                                    alpha:0.4f];
    [self setDynamicObject];
    
    _leftButtonState = NO;
    _rightButtonState = NO;
    num=145;
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:self.appDelegate.city forKey:@"city"];
    [dic setObject:self.appDelegate.userName  forKey:@"userName"];
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:[NSString stringWithFormat:@"%@/GetStreetsList",self.appDelegate.http]
                          jsonDictionary:dic
                              controller:self
                            withSelector:@selector(GetStreetsListResult:)];
    
    self.leftImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *pinchRec = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(openImage:)];
    pinchRec.delegate = self;
    [self.leftImageView addGestureRecognizer:pinchRec];
    
    self.rightImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *pinchRec2 = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(openImage:)];
    pinchRec.delegate = self;
    [self.rightImageView addGestureRecognizer:pinchRec2];
    
    arrimg=[[NSMutableArray alloc]init];
    
    __weak typeof(self) weakSelf = self;\
    self.IVC = [[imagePreviewViewController alloc] init];
    self.IVC.isActiveretakeButton = YES;
    self.IVC.retackePicktureAction = ^{[weakSelf img_click:weakSelf];};
    numImg = 0;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lockScreen) name:@"lockedScreen" object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(status == AVAuthorizationStatusAuthorized) {
        NSLog(@"authorized access");
    } else if(status == AVAuthorizationStatusDenied){
        [self showAlertWithTitle:@"Access to camera denied. Please go to settings and grant access for app."];
    } else if(status == AVAuthorizationStatusRestricted){
        [self showAlertWithTitle:@"Access to camera restricted. Please check the camera."];
    } else if(status == AVAuthorizationStatusNotDetermined){
        NSLog(@"not determined access");
//        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
//            if(granted){
//                NSLog(@"Granted access");
//            } else {
//                NSLog(@"Not granted access");
//            }
//        }];
    }
    if (self.flag) {
        [self OpenCloseKeyBord:num Flag:YES];
        if (self.view.frame.origin.y == num) {
            self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
        }
    }
}

- (void)showAlertWithTitle:(NSString*)title
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:title delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)lockScreen
{
    [_txtNumHouse resignFirstResponder];
    [_txtFildAddress resignFirstResponder];
    [_txtViewDescription resignFirstResponder];
    if([_txtViewDescription.text isEqualToString: @""])
    {
        _txtViewDescription.text=@"תאור";
        _txtViewDescription.textColor=[UIColor lightGrayColor];
    }
    [self OpenCloseKeyBord:num Flag:NO];
}

- (void)openImage:(UITapGestureRecognizer*)sender{
    CGPoint point = [sender locationInView:self.viewContainer];
    UIView *tappedView = [self.viewContainer hitTest:point withEvent:nil];
    if (tappedView.tag == 1) {
        self.IVC.imagePreview = self.leftImageView.image;
        self.senderButton = 1;
    } else {
        self.IVC.imagePreview = self.rightImageView.image;
        self.senderButton = 2;
    }
    [self presentViewController:self.IVC animated:YES completion:nil];
}

-(void)GetStreetsListResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        Street *street=[[Street alloc]init];
        self.listStreets=[street parseStreetListFromJson:result];
        filterStreet=[[NSMutableArray alloc] initWithArray:self.listStreets];
        [autoCompleteTableView reloadData];
    }
    else
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה"
                                                    message:@"נסה שנית מאוחר יותר"
                                                   delegate:self
                                          cancelButtonTitle:@"אישור"
                                          otherButtonTitles: nil];
        [alert show];
    }
}

-(void)setDynamicObject
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd.MM.yyyy HH:mm:ss";
    _lblDateAppeal.text=[NSString stringWithFormat:@"תאריך: %@",[dateFormatter stringFromDate:[NSDate date]]];
    
    _txtFildAddress.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtFildAddress.returnKeyType = UIReturnKeyDone;
    
    [_txtNumHouse setKeyboardType:UIKeyboardTypeNumberPad];
    _txtNumHouse.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtNumHouse.returnKeyType = UIReturnKeyDone;
    
    _txtViewDescription.delegate=self;
    _txtViewDescription.returnKeyType = UIReturnKeyDone;
    _txtViewDescription.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtViewDescription.textColor = [UIColor colorWithRed:200.f/255.f
                                                    green:200.f/255.f
                                                     blue:205.f/255.f
                                                    alpha:1.f];
    
    autoCompleteTableView = self.tableView;
    autoCompleteTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [[autoCompleteTableView layer]setBorderWidth:1.f];
    autoCompleteTableView.scrollEnabled= YES;
    autoCompleteTableView.hidden= YES;
    autoCompleteTableView.rowHeight= 20;
    self.tableView = autoCompleteTableView;
}

-(NSString*)setImageGarbage:(UIImage*)image
{
    NSData *data = nil;
    NSString *imageData = nil;
    NSData *dataForJPEGFile;
    if(image==Nil)
        return @"";
    else
        dataForJPEGFile = UIImageJPEGRepresentation(image, 1.0);
    image=[[UIImage alloc]initWithData:dataForJPEGFile];
    CGSize newSize = CGSizeMake(image.size.width/2, image.size.height/2);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    data=UIImagePNGRepresentation(newImage);
    dataForJPEGFile = UIImageJPEGRepresentation(newImage, 0.1);
    imageData = [dataForJPEGFile base64EncodedString];
    return imageData;
}
- (IBAction)img_click:(id)sender
{
    if (sender == _leftImageButton) {
        _leftButtonState = YES;
    } else if (sender == _rightImageButton)
    {
        _rightButtonState = YES;
    }
    else {
        switch (self.senderButton) {
            case 1:
                _leftButtonState = YES;
                break;
            case 2:
                _rightButtonState = YES;
            default:
                break;
        }
    }
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"בחר באפשרות הרצויה:"
                                                       delegate:self
                                              cancelButtonTitle:@"סגירה"
                                         destructiveButtonTitle:nil otherButtonTitles:@"מצלמה",@"גלרית תמונות",nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)addAppeal_click:(id)sender
{
    if(![_txtFildAddress.text isEqualToString:@""]&&![_txtNumHouse.text isEqualToString:@""]&&![_txtViewDescription.text isEqualToString:@"תאור"])
    {
        if(self.leftImagePicked) {
            [arrimg addObject:[self setImageGarbage:_leftImageView.image]];
        }
        if (self.rightImagePicked) {
            [arrimg addObject:[self setImageGarbage:_rightImageView.image]];
        }
        
        Appeal*appeal=[[Appeal alloc]initWith:_txtViewDescription.text
                                      Address:_txtFildAddress.text
                                  HouseNumber:(int)[_txtNumHouse.text integerValue]
                                     Pictures:arrimg];
        [self.generic showNativeActivityIndicator:self];
        [self.connection connectionToService:[NSString stringWithFormat:@"%@/CreateNewTicket",self.appDelegate.http]
                              jsonDictionary:[appeal parseAppealToDict]
                                  controller:self
                                withSelector:@selector(CreateNewTicketResult:)];
    }
    else
    {
        alertReport=[[UIAlertView alloc]initWithTitle:@""
                                              message:@"חלק משדות החובה חסרים או שתוכנם אינו חוקי. אנא מלאו ונסו שוב"
                                             delegate:self
                                    cancelButtonTitle:@"אישור"
                                    otherButtonTitles: nil];
        [alertReport show];
    }
}

-(void)CreateNewTicketResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        alertReport=[[UIAlertView alloc]initWithTitle:@""
                                              message:@"פניתך התקבלה בהצלחה"
                                             delegate:self
                                    cancelButtonTitle:@"אישור"
                                    otherButtonTitles: nil];
        [alertReport setMessage:@"פניתך התקבלה בהצלחה"];
        [alertReport show];
    }
    else
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView.message isEqualToString:@"פניתך התקבלה בהצלחה"])
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openCamra];
            break;
        case 1:
            [self openGallery];
            break;
        default:
            break;
    }
}

-(void)openCamra
{
    UIImagePickerController *image = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        image.sourceType = UIImagePickerControllerSourceTypeCamera;
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה"
                                                        message:@"מצטערים, אין חיבור של מצלמה במכשיר זה"
                                                       delegate:nil
                                              cancelButtonTitle:@"אשור"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    image.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:image.sourceType];
    NSArray *mediatypes = [NSArray arrayWithObject:image.mediaTypes.firstObject];
    image.mediaTypes = mediatypes;
    image.delegate = self;
    [self presentViewController:image animated:YES completion:nil ];
    
}

-(void)openGallery
{
    UIImagePickerController *image = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
        image.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה"
                                                        message:@"מצטערים הגלריה אינה זמינה"
                                                       delegate:nil
                                              cancelButtonTitle:@"אישור"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    image.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:image.sourceType];
    image.delegate = self;
    [self presentViewController:image animated:YES completion:nil ];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *newImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (_leftButtonState){
        _leftImagePicked = YES;
        _leftButtonState = NO;
        self.leftImageView.image = nil;
        self.leftImageView.image = newImage;
        self.leftImageWidth.constant = newImage.size.width/newImage.size.height*self.leftImageButton.frame.size.height;
        [_leftImageButton setHidden:YES];
        numImg = (numImg == 0) ? 1 : 2;
    }
    else{
        _rightImagePicked = YES;
        _rightButtonState = NO;
        self.rightImageView.image = nil;
        self.rightImageView.image = newImage;
        self.rightImageWidth.constant = newImage.size.width/newImage.size.height*self.rightImageButton.frame.size.height;
        [_rightImageButton setHidden:YES];
        numImg = (numImg == 0) ? 1 : 2;
    }
    _leftButtonState = NO;
    _rightButtonState = NO;
    [self.view setNeedsDisplay];
}

#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return filterStreet.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text =((Street*)filterStreet[indexPath.row]).StreetName;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0];
    [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    cell.backgroundColor=[UIColor lightGrayColor];
    return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    streetName=((Street*)filterStreet[indexPath.row]).StreetName;
    streetId=((Street*)filterStreet[indexPath.row]).StreetId;
    _txtFildAddress.text=streetName;
    [self OpenCloseKeyBord:num Flag:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    _txtFildAddress.text=streetName;
    [self OpenCloseKeyBord:num Flag:NO];
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField.tag==10)
        [textField resignFirstResponder];
    else
    {
        if (textField.tag==2)
        {
            _txtFildAddress.text=@"";
            if(filterStreet.count>7)
                autoCompleteTableView.frame = CGRectMake(_txtFildAddress.frame.origin.x , _txtFildAddress.frame.origin.y+_txtFildAddress.frame.size.height, _txtFildAddress.frame.size.width, 8*20);
            else
                autoCompleteTableView.frame = CGRectMake(_txtFildAddress.frame.origin.x , _txtFildAddress.frame.origin.y+_txtFildAddress.frame.size.height, _txtFildAddress.frame.size.width, filterStreet.count*20);
            [autoCompleteTableView reloadData];
            autoCompleteTableView.hidden= NO;
        }
        if(!self.flag)
            [self OpenCloseKeyBord:-(num) Flag:YES];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _txtFildAddress.text=streetName;
    if([_txtViewDescription.text isEqualToString: @""])
    {
        _txtViewDescription.text=@"תאור";
        _txtViewDescription.textColor=[UIColor lightGrayColor];
    }
    if(self.flag)
        [self OpenCloseKeyBord:num Flag:NO];
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    _txtViewDescription.textColor=[UIColor blackColor];
    if([_txtViewDescription.text isEqualToString:@"תאור"])
        _txtViewDescription.text=@"";
    if(!self.flag)
        [self OpenCloseKeyBord:-(num) Flag:YES];
    return YES;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        if([_txtViewDescription.text isEqualToString: @""])
        {
            _txtViewDescription.text=@"תאור";
            _txtViewDescription.textColor=[UIColor lightGrayColor];
        }
        [self OpenCloseKeyBord:(num) Flag:NO];
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self.view endEditing:YES];
}

-(BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString*ns=[[NSString alloc]init];
    ns=[NSString stringWithFormat:@"%@%@",textField.text,string];
    if([string isEqualToString:@""])
        ns=[[ns substringToIndex:[ns length] - 1] substringFromIndex:0];
    if(textField.tag==2)
    {
        if([ns isEqualToString:@""])
            filterStreet = [[NSMutableArray alloc] initWithArray:_listStreets];
        else
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.StreetName contains[c] %@",ns];
            filterStreet = [NSMutableArray arrayWithArray:[_listStreets filteredArrayUsingPredicate:predicate]];
        }
        
        if(filterStreet.count==0)
            autoCompleteTableView.hidden=YES;
        else
            autoCompleteTableView.hidden=NO;
    }
    
    if (textField.tag == 1) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 5;
    }
    
    [autoCompleteTableView reloadData];
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    if(textField.tag==2)
    {
        textField.text=streetName;
        filterStreet = [[NSMutableArray alloc] initWithArray:_listStreets];
        autoCompleteTableView.hidden=YES;
    }
    return YES;
}

@end
