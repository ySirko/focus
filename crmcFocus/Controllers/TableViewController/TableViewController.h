//
//  TableViewController.h
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "SubViewController.h"

@interface TableViewController : SubViewController

@property int tag;
@property (retain,nonatomic) NSArray *listAppeals;
@property (retain,nonatomic) UITableView *tblAppeals;
@property (strong,nonatomic) UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *searchResult;

@end
