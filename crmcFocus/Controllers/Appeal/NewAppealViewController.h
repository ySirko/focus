//
//  NewAppealViewController.h
//  crmcFocus
//
//  Created by Lior Ronen on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
#import "NSData+Base64.h"

@interface NewAppealViewController : SubViewController 

@end
