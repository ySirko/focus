//
//  TableViewCell.h
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Appeal.h"
#import "ActionLine.h"

@interface TableViewCell : UITableViewCell

@property(retain,nonatomic)UIColor*color;
@property(retain,nonatomic)UIColor*highlightedColor;
@property(retain,nonatomic)UILabel*lblDate;
@property(retain,nonatomic)UILabel*lblDescription;
@property(retain,nonatomic)UILabel*lblName;
@property(retain,nonatomic)UILabel*lblAddress;
@property(retain,nonatomic)UILabel*lblNumber;
@property(retain,nonatomic)UILabel*lblApplicants;
@property(retain,nonatomic)UIButton*btnDetails;
@property(retain,nonatomic)UILabel*lblLine;
@property(retain,nonatomic)UILabel*lblStatus;
@property(retain,nonatomic)UILabel*lblSendDate;
@property(retain,nonatomic)NSString*strColor;
@property (nonatomic) BOOL viewDetails;
@property (nonatomic) int width;

-(void)SetCell:(Appeal*)appeal;
-(void)SetActionLineCell:(ActionLine*)actionLine;
-(void)setReportedInquiriesCell:(Appeal*)appeal;

@end
