//
//  MenuViewController.h
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "SubViewController.h"
#import "SoundMenuViewController.h"

@interface MenuViewController : SubViewController
@property(retain,nonatomic)UIButton*btn1;
@property(retain,nonatomic)UIButton*btn2;
@property(retain,nonatomic)UIButton*btn3;
@property(retain,nonatomic)UIButton*btn4;
@property(retain,nonatomic)UIButton*btn5;
@property(retain,nonatomic)UIButton*btn6;
@property(retain,nonatomic)NSArray*menuItemsArray;
@property(retain,nonatomic)NSArray*buttonsArray;
@property(retain,nonatomic)UILabel*lblVersion;
@property(retain,nonatomic)UILabel*crmc;
@property(retain,nonatomic)UIButton*btnLinkCrmc;
@property (retain,nonatomic)NSString* msgNotification;
@property(retain,nonatomic)UIImageView*imgNumbrer1;
@property(retain,nonatomic)UIImageView*imgNumbrer2;
@property(retain,nonatomic)UIImageView*imgNumbrer3;
@property(retain,nonatomic)UIImageView*imgNumbrer4;
@property(retain,nonatomic)UIImageView*imgNumbrer5;
@property(retain,nonatomic)UIImageView*imgNumbrer6;
@property(retain,nonatomic)UILabel*lblNumbrer1;
@property(retain,nonatomic)UILabel*lblNumbrer2;
@property(retain,nonatomic)UILabel*lblNumbrer3;
@property(retain,nonatomic)UILabel*lblNumbrer4;
@property(retain,nonatomic)UILabel*lblNumbrer5;
@property(retain,nonatomic)UILabel*lblNumbrer6;
@property(retain,nonatomic)NSArray*imgsArray;
@property(retain,nonatomic)NSArray*lblsArray;
@property (strong, nonatomic) NSDictionary *notificationDictionary;

@end
