//
//  SubViewController.m
//  crmcFocus
//
//  Created by Lior Ronen on 10/27/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "SubViewController.h"
#import "MenuViewController.h"
#import "LoginViewController.h"
#import "SearchViewController.h"
#import "GAITrackedViewController.h"
#import "SoundMenuViewController.h"

@interface SubViewController ()

@end

@implementation SubViewController

- (instancetype)init
{
    if (self = [super init]) {
        _isHideNumberOfTickets = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _width=self.view.frame.size.width;
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _connection=[[Connection alloc]init];
    _generic=[[Generic alloc]init];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    self.view.backgroundColor=[UIColor whiteColor];
    _Blue=[UIColor colorWithRed:33.0f/255.0f green:64.0f/255.0f blue:154.0f/255.0f alpha:1.0f];
    _Pink=[UIColor colorWithRed:239.0f/255.0f green:19.0f/255.0f blue:121.0f/255.0f alpha:1.0f];
    _Green=[UIColor colorWithRed:23.0f/255.0f green:184.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
    _Orange=[UIColor colorWithRed:255.0f/255.0f green:142.0f/255.0f blue:12.0f/255.0f alpha:1.0f];
    _Purple=[UIColor colorWithRed:76.0f/255.0f green:0.0f/255.0f blue:121.0f/255.0f alpha:1.0f];
    _Gray=[UIColor colorWithRed:128.0f/255.0f green:130.0f/255.0f blue:133.0f/255.0f alpha:1.0f];
    _HighlightedBlue=[UIColor colorWithRed:30.0f/255.0f green:57.0f/255.0f blue:141.0f/255.0f alpha:1.0f];
    _HighlightedPink=[UIColor colorWithRed:182.0f/255.0f green:35.0f/255.0f blue:103.0f/255.0f alpha:1.0f];
    _HighlightedGreen=[UIColor colorWithRed:0.0f/255.0f green:166.0f/255.0f blue:81.0f/255.0f alpha:1.0f];
    _HighlightedOrange=[UIColor colorWithRed:246.0f/255.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0f];
    _HighlightedRed=[UIColor colorWithRed:186.0f/255.0f green:0.0f/255.0f blue:23.0f/255.0f alpha:1.0f];
    _HighlightedPurple=[UIColor colorWithRed:76.0f/255.0f green:0.0f/255.0f blue:121.0f/255.0f alpha:1.0f];
    _HighlightedGray=[UIColor colorWithRed:99.0f/255.0f green:100.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
    [self SubViewSetControllers];
}

- (void)SubViewSetControllers
{
    _lblbackgroung=[[UILabel alloc]initWithFrame:CGRectMake(0, 150, CGRectGetWidth([UIScreen mainScreen].bounds), 260)];
    if (CGRectGetHeight([UIScreen mainScreen].bounds) > 480) {
        if(CGRectGetWidth([UIScreen mainScreen].bounds) == 375)
            _lblbackgroung.frame=CGRectMake(0, 185, CGRectGetWidth([UIScreen mainScreen].bounds), 310);
        else
            _lblbackgroung.frame=CGRectMake(0, 165, CGRectGetWidth([UIScreen mainScreen].bounds), 310);
    }
    _lblbackgroung.alpha = 0.4;
    [self.view addSubview:_lblbackgroung];
    
    _background = [[UIImageView alloc]initWithFrame:CGRectMake(0,CGRectGetHeight([UIScreen mainScreen].bounds) - CGRectGetWidth([UIScreen mainScreen].bounds) * 35/32, CGRectGetWidth([UIScreen mainScreen].bounds), CGRectGetWidth([UIScreen mainScreen].bounds) * 35/32)];
    [_background setAlpha:0.5];
    _background.image=[UIImage imageNamed:@"Background.png"];
    [self.view addSubview:_background];
    [self.view sendSubviewToBack:_background];
    
    _lblBar = [[UILabel alloc]initWithFrame:CGRectMake(0, 18, CGRectGetWidth([UIScreen mainScreen].bounds), 31)];
    _lblBar.backgroundColor=[UIColor lightGrayColor];
    [self.view addSubview:_lblBar];
    
    if(![self isKindOfClass:[LoginViewController class]])
    {
        _btnSettings=[[UIButton alloc]initWithFrame:CGRectMake(10, 20, 27, 27)];
        [_btnSettings addTarget:self action:@selector(btnSettingsClick:) forControlEvents:UIControlEventTouchUpInside];
        [_btnSettings setImage:[UIImage imageNamed:@"logOutIcon.png"] forState:normal];
        [self.view addSubview:_btnSettings];
        
        UIButton *soundMenuButton = [[UIButton alloc] init];
        soundMenuButton=[[UIButton alloc]initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds) - 35, 20, 27, 27)];
        [soundMenuButton addTarget:self action:@selector(openSoundMenu:) forControlEvents:UIControlEventTouchUpInside];
        [soundMenuButton setImage:[[UIImage imageNamed:@"settings.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forState:normal];
        [self.view addSubview:soundMenuButton];
    
        if(![self isKindOfClass:[MenuViewController class]]){
            _btnPopUp=[[UIButton alloc]initWithFrame:CGRectMake(55, 20, 27, 27)];
            [_btnPopUp addTarget:self action:@selector(btnPopUpClick:) forControlEvents:UIControlEventTouchUpInside];
            [_btnPopUp setImage:[UIImage imageNamed:@"BackIcon.png"] forState:normal];
            [self.view addSubview:_btnPopUp];
            _btnMenu=[[UIButton alloc]initWithFrame:CGRectMake(100, 23, 27, 20)];
            [_btnMenu addTarget:self action:@selector(btnHomePageClick:) forControlEvents:UIControlEventTouchUpInside];
            [_btnMenu setImage:[UIImage imageNamed:@"HomeIcon.png"] forState:normal];
            [self.view addSubview:_btnMenu];
        }
    }
    
    _image=[[UIImageView alloc]initWithFrame:CGRectMake(CGRectGetWidth([UIScreen mainScreen].bounds) * 91/320, 50, CGRectGetWidth([UIScreen mainScreen].bounds) * 220/320, CGRectGetWidth([UIScreen mainScreen].bounds) * 49/320)];
    _image.image=[UIImage imageNamed:@"Image.png"];
    [self.view addSubview:_image];
    
    _imageLogo = [[UIImageView alloc]initWithFrame:CGRectMake(10, 58, CGRectGetWidth([UIScreen mainScreen].bounds) * 60/320, CGRectGetWidth([UIScreen mainScreen].bounds) * 42/320)];
    _imageLogo.image = [UIImage imageNamed:@"logo.png"];
    [self.view addSubview:_imageLogo];
    
    _lbl=[[UILabel alloc]initWithFrame:CGRectMake(0,_image.frame.origin.y+_image.frame.size.height+4,  CGRectGetWidth([UIScreen mainScreen].bounds), 29)];
    _lbl.backgroundColor=_color;
    [self.view addSubview:_lbl];
    
    _lblTitel=[[UILabel alloc]initWithFrame:CGRectMake(0, _image.frame.origin.y+_image.frame.size.height+4,CGRectGetWidth([UIScreen mainScreen].bounds) - 20, 29)];
    _lblTitel.font=[UIFont fontWithName:@"Helvetica Bold" size:16];
    _lblTitel.textColor=[UIColor whiteColor];
    _lblTitel.textAlignment=NSTextAlignmentCenter;
    _lblTitel.text=_header;
    [self.view addSubview:_lblTitel];
}

-(void)openSoundMenu:(UIButton*)sender
{
    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    SoundMenuViewController *presentController = [SoundMenuViewController new];
    presentController.isHideNumberOfTickets = self.isHideNumberOfTickets;
    
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.f) {
        presentController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    
    [self presentViewController:presentController animated:YES completion:nil];
    
}

-(void)OpenCloseKeyBord:(int)num Flag:(BOOL)openClose
{
    //[self.view endEditing:YES];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    
    if(!openClose) {
        [self.view endEditing:YES];
        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } else {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+num, self.view.frame.size.width, self.view.frame.size.height);
    }
    self.flag=openClose;
    [UIView commitAnimations];
}

-(IBAction)btnSettingsClick:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLogined"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    LoginViewController*view=[[LoginViewController alloc]init];
    view.header=@"כניסת משתמש";
    view.color=self.Purple;
    [self.navigationController pushViewController:view animated:YES];
}

-(IBAction)btnSearchClick:(id)sender
{
    SearchViewController*view=[[SearchViewController alloc]init];
    view.header=@"כניסת משתמש";
    view.color=self.Purple;
    [self.navigationController pushViewController:view animated:YES];
}


-(IBAction)btnPopUpClick:(id)sender
{
    if ([self.navigationController.viewControllers count] > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self btnHomePageClick:self];
    }
}

-(IBAction)btnHomePageClick:(id)sender
{
    MenuViewController*view=[[MenuViewController alloc]init];
    view.color=[UIColor lightGrayColor];
    NSMutableArray *viewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    [viewControllers replaceObjectAtIndex:0 withObject:view];
    [self.navigationController setViewControllers:viewControllers];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
