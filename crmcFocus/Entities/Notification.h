//
//  Notification.h
//  crmcFocus
//
//  Created by MyMac on 11/12/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notification : NSObject
@property int pushType;
@property(retain,nonatomic)NSString* msg;
@property int ticketId;
@property(retain,nonatomic)NSString*actionLineId;
@property(retain,nonatomic)NSString*title;
-(Notification*)parseNotificationListFromJson:(NSDictionary*) dic;
@end
