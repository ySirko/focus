//
//  SearchViewController.m
//  פוקוס לפקחים
//
//  Created by MyMac on 12/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _txtNum=[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 150, self.view.frame.size.width/2, 20)];
    _txtNum.backgroundColor=[UIColor redColor];
    _txtNum.delegate=self;
    [self.view addSubview:_txtNum];
    
    _txtAddress=[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 200, self.view.frame.size.width/2, 20)];
    _txtAddress.backgroundColor=[UIColor redColor];
    _txtAddress.delegate=self;
    [self.view addSubview:_txtAddress];
    
    _txtSubject=[[UITextField alloc]initWithFrame:CGRectMake(self.view.frame.size.width/6, 250, self.view.frame.size.width/2, 20)];
    _txtSubject.backgroundColor=[UIColor redColor];
    _txtSubject.delegate=self;
    [self.view addSubview:_txtSubject];
    
    _btnSearchh=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4, 320, self.view.frame.size.width/2, 20)];
    _btnSearchh.backgroundColor=[UIColor redColor];
    [_btnSearchh setTitle:@"חיפוש" forState:normal];
    [_btnSearchh addTarget:self action:@selector(btnSearchhClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnSearchh];
}

-(IBAction)btnSearchhClick:(id)sender
{
    SearchViewController*view=[[SearchViewController alloc]init];
    view.header=@"כניסת משתמש";
    view.color=self.Purple;
    [self.navigationController pushViewController:view animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
     [textField resignFirstResponder];
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
