//
//  SearchViewController.h
//  פוקוס לפקחים
//
//  Created by MyMac on 12/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
@interface SearchViewController : SubViewController<UITextFieldDelegate>
@property(retain,nonatomic)UITextField*txtNum;
@property(retain,nonatomic)UITextField*txtAddress;
@property(retain,nonatomic)UITextField*txtSubject;
@property(retain,nonatomic)UIButton*btnSearchh;
@end
