//
//  DetailsViewController.m
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "DetailsViewController.h"
#import "AppealTreatmentsViewController.h"
#import "NSData+Base64.h"
#import "ActionLine.h"
#import <MapKit/MapKit.h>

#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 320.0f
#define CELL_CONTENT_MARGIN 10.0f

@interface DetailsViewController () <UITableViewDataSource, UITableViewDelegate>
{
    int OriganY;
    BOOL flagImg;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableViewDataSource;

@end

@implementation DetailsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    if(self.view.frame.size.height>480)
    {
        if(self.width==375)
            OriganY=190;
        else
            OriganY=160;
    }
    else
        OriganY=140;
    
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:self.appDelegate.city forKey:@"city"];
    [dic setObject:self.appDelegate.userName  forKey:@"userName"];
    [dic setObject:[NSNumber numberWithInt:_appeal.ID] forKey:@"ticketId"];
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:[NSString stringWithFormat:@"%@/GetTicketInfoById",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(GetTicketInfoByIdResult:)];
    if(_notification)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:_notification delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 146, self.view.frame.size.width, 180)];
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    _lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0,self.view.frame.size.height-10,self.width,4)];
    _lblLine.backgroundColor=self.color;
    [self.view addSubview:self.lblLine];
}

-(void)AddActionLineResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page_ = (int)round(scrollView.contentOffset.x / scrollView.frame.size.width);
    _pgcontrol.currentPage = page_;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.tableViewDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    NSString *string = self.tableViewDataSource[indexPath.row];
    NSString *newString = [string stringByReplacingOccurrencesOfString:@"; " withString:@";\n"];

    
    cell.textLabel.text = newString;
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.numberOfLines = 0;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8) {
        NSString *text = [self.tableViewDataSource objectAtIndex:[indexPath row]];
        // Get a CGSize for the width and, effectively, unlimited height
        CGSize constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
        // Get the size of the text given the CGSize we just made as a constraint
        CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:FONT_SIZE] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        // Get the height of our measurement, with a minimum of 44 (standard cell size)
        CGFloat height = MAX(size.height, 20.0f);
        // return the height, with a bit of extra padding in
        return height + (CELL_CONTENT_MARGIN * 2);
    } else
        return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

#pragma mark - IBActions

- (IBAction)btnCallClick:(id)sender
{
    NSString *phoneNumber = [(UIButton *)sender titleLabel].text;
    
    if (!phoneNumber.length || !phoneNumber) {
        return;
    }
    
    UIApplication *myApp = [UIApplication sharedApplication];
    NSString *theCall = [NSString stringWithFormat:@"tel://%@", phoneNumber];
    NSLog(@"making call with %@",theCall);
    [myApp openURL:[NSURL URLWithString:theCall]];
}

- (IBAction)btnCareAppealClick:(id)sender
{
    AppealTreatmentsViewController* view=[[AppealTreatmentsViewController alloc]init];
    view.appeal=_appeal;
    view.color=self.color;
    view.strColor=self.strColor;
    view.highlightedColor=self.highlightedColor;
    view.header= [NSString stringWithFormat:@"טיפול בפניה %d",view.appeal.ID];
    [self.navigationController pushViewController:view animated:YES];
}

- (IBAction)btnWazeClick:(id)sender
{
    if (!_appeal.Address || !_appeal.Address.length) {
        return;
    }
    CLLocationCoordinate2D location=[self geoCodeUsingAddress:[_appeal.Address substringWithRange:NSMakeRange(0, [_appeal.Address rangeOfString:@"הערות"].location)]];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) {
        NSString *urlStr = [NSString stringWithFormat:@"waze://?ll=%f,%f&navigate=yes", location.latitude, location.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
    }
}

#pragma mark - Private Methods

- (void)dataSource
{
    self.tableViewDataSource = [[NSMutableArray alloc] init];
    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"נושא: %@",_appeal.Subject]];
    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"תאור: %@",_appeal.Descriptionn]];
    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"כתובת: %@",_appeal.Address]];
    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"שם הפונה: %@ %@",_appeal.ReporterFirstName,_appeal.ReporterLastName]];
    //    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"טלפון נייח: %@",_appeal.ReporterHomePhoneNumber]];
    //    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"טלפון נייד: %@",_appeal.ReporterCellPhoneNumber]];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm dd/MM/yyyy"];
    [self.tableViewDataSource addObject:[NSString stringWithFormat:@"תאריך פתיחה: %@",[dateFormatter stringFromDate:_appeal.OpenDate]]];
    //[self.tableViewDataSource addObject:[NSString stringWithFormat:@"זמן צפי לטיפול בפניה :%@",[dateFormatter stringFromDate:_appeal.ForeCast]]];
}

- (void)setBottomElements
{
    _pgcontrol = [[UIPageControl alloc] initWithFrame:CGRectZero];
    _pgcontrol.pageIndicatorTintColor = [UIColor colorWithWhite:0.8 alpha:1];
    _pgcontrol.currentPageIndicatorTintColor = [UIColor colorWithWhite:0.6 alpha:1];
    _pgcontrol.numberOfPages = _appeal.arrPictures.count;
    _pgcontrol.currentPage = 0;
    [_pgcontrol sizeToFit];
    _pgcontrol.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-22);
    [self.view addSubview:_pgcontrol];
}

- (void)configureScrollViewWithRect:(CGRect)rect
{
    _scrlImag=[[UIScrollView alloc]initWithFrame:rect];
    _scrlImag.contentSize= CGSizeMake(_scrlImag.frame.size.width*_appeal.arrPictures.count, _scrlImag.frame.size.height);
    _scrlImag.pagingEnabled=true;
    _scrlImag.bounces=false;
    _scrlImag.delegate=self;
    _scrlImag.showsHorizontalScrollIndicator=false;
    _scrlImag.showsVerticalScrollIndicator=false;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapGestureCaptured:)];
    [singleTap setDelaysTouchesBegan : YES];
    singleTap.numberOfTapsRequired = 2;
    singleTap.numberOfTouchesRequired = 1;
    [_scrlImag addGestureRecognizer:singleTap];
    [self.view addSubview:_scrlImag];
    
    int index=0;
    for(NSString*ss in _appeal.arrPictures)
    {
        UIImageView*i=[[UIImageView alloc]initWithFrame:CGRectMake(_scrlImag.bounds.size.width * index-6,0,_scrlImag.bounds.size.width,_scrlImag.bounds.size.height)];
        i.contentMode = UIViewContentModeScaleAspectFit;
        i.image=[UIImage imageWithData:[NSData dataFromBase64String:[NSString stringWithFormat:@"%@", ss]]];
        [_scrlImag addSubview:i];
        index++;
    }
}

-(void)GetTicketInfoByIdResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        _appeal=[Appeal parseAppealListFromJson:result].firstObject;
        [self dataSource];
        [self.tableView reloadData];
        if(_tag==4)
            [self SetReportedInquiriesControllers];
        else
            [self SetControllers];
    }
    else
    {
        NSLog(@"ERROR");
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

-(void)SetReportedInquiriesControllers
{
    [self configureScrollViewWithRect:CGRectMake(0, self.tableView.frame.origin.y+ self.tableView.frame.size.height+5, self.view.frame.size.width, self.lblLine.frame.origin.y - self.tableView.frame.origin.y - self.tableView.frame.size.height - 26)];
    [self setBottomElements];
}

-(void)SetControllers
{
    _btnCareAppeal=[[UIButton alloc]initWithFrame:CGRectMake(12, self.view.frame.size.height-38, 100, 26)];
    [_btnCareAppeal setTitle:@"טיפול בפניה" forState:normal];
    _btnCareAppeal.layer.cornerRadius=2;
    _btnCareAppeal.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:18];
    [_btnCareAppeal setBackgroundColor:self.color];
    [_btnCareAppeal addTarget:self action:@selector(btnCareAppealClick:) forControlEvents:UIControlEventTouchUpInside];
    
    if (_tag != 3) {
        [self.view addSubview:_btnCareAppeal];
    }
    _btnWaze=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width-112, self.view.frame.size.height-38, 100, 26)];
    [_btnWaze setTitle:@"נווט באמצעות waze" forState:normal];
    _btnWaze.layer.cornerRadius=2;
    _btnWaze.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:18];
    [_btnWaze setBackgroundColor:self.color];
    [_btnWaze addTarget:self action:@selector(btnWazeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnWaze];
    [self configureScrollViewWithRect:CGRectMake(0, self.tableView.frame.origin.y+self.tableView.frame.size.height+5, self.view.frame.size.width, self.btnWaze.frame.origin.y-self.tableView.frame.origin.y-self.tableView.frame.size.height-10)];
    
    [self setBottomElements];
}

- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

- (void)singleTapGestureCaptured:(UITapGestureRecognizer *)gesture
{
    int index=0;
    
    if(flagImg)
    {
        if(_tag!=4)
            _scrlImag.frame=CGRectMake(0, self.tableView.frame.origin.y+self.tableView.frame.size.height+5, self.view.frame.size.width, self.btnWaze.frame.origin.y-self.tableView.frame.origin.y-self.tableView.frame.size.height-10);
        
        else
            _scrlImag.frame=CGRectMake(0, self.tableView.frame.origin.y+ self.tableView.frame.size.height+5, self.view.frame.size.width, self.lblLine.frame.origin.y - self.tableView.frame.origin.y - self.tableView.frame.size.height - 26);
        
        _scrlImag.contentSize= CGSizeMake(_scrlImag.frame.size.width*_appeal.arrPictures.count, _scrlImag.frame.size.height);
    }
    else
        _scrlImag.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    for(UIView *subview in [_scrlImag subviews])
    {
        subview.frame=CGRectMake(_scrlImag.bounds.size.width * index,0,_scrlImag.bounds.size.width,_scrlImag.bounds.size.height);
        index++;
    }
    _scrlImag.contentSize= CGSizeMake(_scrlImag.frame.size.width*_appeal.arrPictures.count, _scrlImag.frame.size.height);
    flagImg=!flagImg;
    _pgcontrol.hidden=!_pgcontrol.hidden;
}


@end
