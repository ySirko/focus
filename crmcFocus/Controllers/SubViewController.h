//
//  SubViewController.h
//  crmcFocus
//
//  Created by Lior Ronen on 10/27/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Connection.h"
#import "Generic.h"
#import "AppDelegate.h"
#import "GAITrackedViewController.h"
#import "SoundMenuViewController.h"

@interface SubViewController :GAITrackedViewController
@property (retain,nonatomic)AppDelegate*appDelegate;
@property(retain,nonatomic)Connection*connection;
@property(retain,nonatomic)Generic*generic;
@property(retain,nonatomic)UIColor*Green;
@property(retain,nonatomic)UIColor*HighlightedGreen;
@property(retain,nonatomic)UIColor*Blue;
@property(retain,nonatomic)UIColor*HighlightedBlue;
@property(retain,nonatomic)UIColor*Orange;
@property(retain,nonatomic)UIColor*HighlightedOrange;
@property(retain,nonatomic)UIColor*Pink;
@property(retain,nonatomic)UIColor*HighlightedPink;
@property(retain,nonatomic)UIColor*Purple;
@property(retain,nonatomic)UIColor*HighlightedPurple;
@property(retain,nonatomic)UIColor*Gray;
@property(retain,nonatomic)UIColor*HighlightedGray;
@property(retain,nonatomic)UIColor*color;
@property(retain,nonatomic)UIColor*highlightedColor;
@property(retain,nonatomic)UILabel*lblBar;
@property(retain,nonatomic)UIButton*btnMenu;
@property(retain,nonatomic)UIButton*btnSettings;
@property(retain,nonatomic)UIButton*btnPopUp; 
@property(retain,nonatomic)UIButton*btnSearch;
@property(retain,nonatomic)UIImageView*imageLogo;
@property(retain,nonatomic)UIImageView*image;
@property(retain,nonatomic)UILabel*lblTitel;
@property(retain,nonatomic)NSString*header;
@property(retain,nonatomic)UIColor*HighlightedRed;
@property(retain,nonatomic)UIImageView*background;
@property(retain,nonatomic)UILabel*lbl;
@property(retain,nonatomic)NSString*strColor;

@property (assign, nonatomic) BOOL isHideNumberOfTickets;

@property BOOL flag;
@property (retain, nonatomic) UILabel*lblbackgroung;
@property int width;
-(void)OpenCloseKeyBord:(int)num Flag:(BOOL)openClose;
@end
