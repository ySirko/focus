//
//  Appeal.m
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "Appeal.h"
#import "AppDelegate.h"
@implementation Appeal

-(id)initWith:(NSString*)description Address:(NSString*)address HouseNumber:(int)houseNumber Pictures:(NSMutableArray*)pictures
{
    _Address=address;
    _HouseNumber=houseNumber;
    _Descriptionn=description;
    _arrPictures = pictures;
    return self;
}

+ (NSMutableArray*)parseAppealListFromJson:(NSString*) strJson
{
    NSError* error;
    NSDictionary* dic=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arr=[[NSMutableArray alloc]init];
    for (NSDictionary* dict in [dic objectForKey:@"Entities"])
        [arr addObject:[Appeal parseAppealFromJson:dict]];
  

    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"SendDate" ascending:NO];
    NSArray* sortedArray=[arr sortedArrayUsingDescriptors:@[sort]];
    arr=[[NSMutableArray alloc]initWithArray:sortedArray];
    return  arr;
}

+ (Appeal*)parseAppealFromJson:(NSDictionary*)dic
{
    Appeal*appeal=[[Appeal alloc]init];
    appeal.ActionLines=[dic objectForKey:@"ActionLines"];
    appeal.AlreadyRead=(BOOL)[[dic objectForKey:@"AlreadyRead"] boolValue];
    appeal.DepartmentName=[dic objectForKey:@"DepartmentName"];
    appeal.Descriptionn=[dic objectForKey:@"Description"];
    appeal.ForeCast = [Appeal getDateFromJSON:[dic objectForKey:@"ForeCast"]];
    appeal.HouseNumber=(int)[[dic objectForKey:@"HouseNumber"] integerValue];
    appeal.ID=(int)[[dic objectForKey:@"ID"] integerValue];
    appeal.OpenDate=[Appeal getDateFromJSON:[dic objectForKey:@"OpenDate"]];
    appeal.SendDate=[Appeal getDateFromStr:[dic objectForKey:@"SendDate"]];
    appeal.arrPictures=[[NSMutableArray alloc]init];

    for (NSDictionary* dict in [dic objectForKey:@"Pictures"])
        [appeal.arrPictures addObject:[dict objectForKey:@"GraphicBytesBase64"]];
        appeal.ReporterCellPhoneNumber=[dic objectForKey:@"ReporterCellPhoneNumber"];
        appeal.ReporterFirstName=[dic objectForKey:@"ReporterFirstName"];
        appeal.ReporterHomePhoneNumber=[dic objectForKey:@"ReporterHomePhoneNumber"];
        appeal.ReporterLastName=[dic objectForKey:@"ReporterLastName"];
        appeal.SoundFile=[dic objectForKey:@"SoundFile"];
        appeal.StatusId=(int)[[dic objectForKey:@"StatusId"] integerValue];
        appeal.StatusName=[dic objectForKey:@"StatusName"];
        appeal.Subject=[dic objectForKey:@"Subject"];
        appeal.TempID=[dic objectForKey:@"TempID"];
        appeal.Address=[dic objectForKey:@"Address"];
    return appeal;
}

- (NSDictionary*)parseAppealToDict
{
    AppDelegate*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary* dictt=[[NSMutableDictionary alloc]init];
    [dictt setObject:appDelegate.city forKey:@"city"];
    [dictt setObject:appDelegate.userName  forKey:@"userName"];
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:_Address forKey:@"Address"];
    [dict setObject:_Descriptionn forKey:@"Description"];
    [dict setObject:[NSNumber numberWithInt:_HouseNumber] forKey:@"HouseNumber"];
    
    
    NSMutableArray*list=[[NSMutableArray alloc]init];
    for(NSString*img in _arrPictures)
    {
        NSMutableDictionary* dic=[[NSMutableDictionary alloc]init];
        [dic setObject:img forKey:@"GraphicBytesBase64"];
        [list addObject:dic];
    }
    [dict setObject:list forKey:@"Pictures"];
    [dictt setObject:dict forKey:@"ticket"];
    return dictt;
}
-(NSString*)convertDate:(NSDate*)date
{
    NSDate* d= date;
    NSDateFormatter *formatterYear = [[NSDateFormatter alloc] init];
    [formatterYear setDateFormat:@"dd/MM/YYYY"];
    NSString *stringFromDateYear = [formatterYear stringFromDate:d];
    
    
    return stringFromDateYear;
}

+ (NSDate*)getDateFromJSON:(NSString *)dateString
{
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

+ (NSDate*)getDateFromStr:(NSString *)dateString
{

    NSDate* dateFromString=[[NSDate alloc]init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    if (!(dateString == (NSString *)[NSNull null]))
            dateFromString = [formatter dateFromString:dateString];
    return dateFromString;
}
@end


