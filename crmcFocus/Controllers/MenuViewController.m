//
//  MenuViewController.m
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuItem.h"
#import "TableViewController.h"
#import "NewAppealViewController.h"
#import "InformationViewController.h"
#import "LoginViewController.h"
#import "DetailsViewController.h"
#import "Notification.h"
#import "AppealTreatmentsViewController.h"

@interface MenuViewController () <UIAlertViewDelegate>
{
    int XOrigion;
    int YOrigion;
    int heightSize;
    int widthSize;
    int space;
}

@property (strong, nonatomic) Notification *notification;

@end

@implementation MenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.notificationDictionary) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Push notification received" message:@" האם ברצונך לעבור לפניה?"
 delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
        [alert show];
    }
    
    self.screenName = @"Menu Screen";
    space=10;
    
    if (self.view.frame.size.width==414) {
        //iPhone 5.5"
        YOrigion=220;
        XOrigion=72;
        widthSize=130;
        heightSize=130;}
    
    if (self.view.frame.size.width==375)
    {
        //iPhone 4.7"
        YOrigion=180;
        XOrigion=52.5;
        widthSize=130;
        heightSize=130;
    }
    
    if(self.view.frame.size.width==320 && self.view.frame.size.height>480)
    {
        //iPhone 4.0"
        YOrigion=165;
        XOrigion=45;
        widthSize=110;
        heightSize=110;
    }
    else if ( self.view.frame.size.height == 480)
    {
        //iPhone 3.5"
        YOrigion=150;
        XOrigion=65;
        widthSize=90;
        heightSize=90;
    }
    [self SetMenu];
    if(_msgNotification)
    {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:_msgNotification delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.lblTitel.text = [NSString stringWithFormat:@"שם משתמש %@",self.appDelegate.userName];
    [self reLogin];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
    } else {
        [self showNotification:self.notificationDictionary];
    }
}

- (void)showNotification:(NSDictionary *)note
{
    NSLog(@"ENTERED %@",note);
    self.notification = [[Notification alloc] init];
    self.notification.ticketId=(int)[[note objectForKey:@"TicketId"]integerValue];
    self.notification.title=[note objectForKey:@"Title"];
    self.notification.pushType=(int)[[note objectForKey:@"PushType"]integerValue];
    
    switch (self.notification.pushType)
    {
        case 1:
        {
            [self pushDetailsViewController];
            break;
        }
        case 2:
        {
            [self pushApealTreatmentsViewController];
            break;
        }
        case 3:
        {
            [self pushDetailsViewController];
            break;
        }
        default:
            break;
    }
}

- (void)pushDetailsViewController
{
    DetailsViewController *view=[[DetailsViewController alloc]init];
    view.appeal=[[Appeal alloc]init];
    view.appeal.ID = self.notification.ticketId;
    view.color=[UIColor colorWithRed:255.0f/255.0f green:142.0f/255.0f blue:12.0f/255.0f alpha:1.0f];
    view.highlightedColor=[UIColor colorWithRed:246.0f/255.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0f];
    view.strColor=@"orange";
    view.header=@"פרטי פנייה";
    view.tag=2;
    view.pushType = self.notification.pushType;
    view.notification=self.notification.msg;
    [self.navigationController pushViewController:view animated:YES];
}

- (void)pushApealTreatmentsViewController
{
    AppealTreatmentsViewController *view=[[AppealTreatmentsViewController alloc]init];
    view.appeal=[[Appeal alloc]init];
    view.appeal.ID=self.notification.ticketId;
    view.color=[UIColor colorWithRed:255.0f/255.0f green:142.0f/255.0f blue:12.0f/255.0f alpha:1.0f];
    view.highlightedColor=[UIColor colorWithRed:246.0f/255.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0f];
    view.strColor=@"orange";
    view.header=@"טיפול בפניה";
    view.notification=self.notification.msg;
    [self.navigationController pushViewController:view animated:YES];
}

- (void)reLogin {
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:self.appDelegate.city forKey:@"city"];
    [dic setObject:self.appDelegate.userName forKey:@"username"];
    [dic setObject:[NSString stringWithFormat:@"%@#0",self.appDelegate.password]  forKey:@"password"];
    [self.generic showNativeActivityIndicator:self];
    if(self.appDelegate.dt){
        [dic setObject:self.appDelegate.dt forKey:@"phoneNumber"];
    }
    else {
        [dic setObject:@"" forKey:@"phoneNumber"];
    }
    NSString *pushSound = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushNotificationSound"];
    
    if (pushSound){
        [dic setObject:pushSound forKey:@"sound"];
    }
    else {
        NSString *defSound = @"default";
        [dic setObject:defSound forKey:@"sound"];
    }
    [self.connection connectionToService:[NSString stringWithFormat:@"%@/Login",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(LoginResult:)];
}

-(void)LoginResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result rangeOfString:@"UserNotFound"].location==NSNotFound&&!([result rangeOfString:@"Entities"].location==NSNotFound)&&!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        NSError* error;
        NSDictionary* dic=[NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
        for (NSDictionary* dict in [dic objectForKey:@"Entities"])
        {
            _lblNumbrer1=[[UILabel alloc]init];
            _lblNumbrer2=[[UILabel alloc]init];
            _lblNumbrer3=[[UILabel alloc]init];
            _lblNumbrer1.text= [NSString stringWithFormat:@"%d",(int)[[dict objectForKey:@"totalNew"] integerValue]];
            _lblNumbrer2.text= [NSString stringWithFormat:@"%d",(int)[[dict objectForKey:@"totalOpened"] integerValue]];
//          _lblNumbrer3.text = @"12313413";
            _lblNumbrer3.text= [NSString stringWithFormat:@"%d",(int)[[dict objectForKey:@"totalClosed"] integerValue]];
        }
        [self SetControllers];
    }
    else
    {
        if(!([result rangeOfString:@"UserNotFound"].location==NSNotFound))
        {
            LoginViewController*view=[[LoginViewController alloc]init];
            [self.navigationController pushViewController:view animated:YES];
        }
        else if([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound)
        {
            UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
            [alert show];
        }
    }
}

-(void)SetMenu
{
    MenuItem*menuItem1=[[MenuItem alloc]init];
    menuItem1.tag=1;
    menuItem1.name=@"פניות חדשות";
    menuItem1.color=[UIColor redColor];
    menuItem1.highlightedColor=self.HighlightedRed;
    menuItem1.icon=[UIImage imageNamed:@"IconNew.png"];
    menuItem1.strColor=@"red";
    
    MenuItem*menuItem2=[[MenuItem alloc]init];
    menuItem2.tag=2;
    menuItem2.name=@"פניות פתוחות";
    menuItem2.color=self.Green;
    menuItem2.highlightedColor=self.HighlightedGreen;
    menuItem2.icon=[UIImage imageNamed:@"IconOpen.png"];
    menuItem2.strColor=@"green";
    
    MenuItem*menuItem3=[[MenuItem alloc]init];
    menuItem3.tag=3;
    menuItem3.name=@"פניות סגורות";
    menuItem3.color=self.Blue;
    menuItem3.highlightedColor=self.HighlightedBlue;
    menuItem3.icon=[UIImage imageNamed:@"IconClose.png"];
    menuItem3.strColor=@"blue";
    
    MenuItem*menuItem4=[[MenuItem alloc]init];
    menuItem4.tag=4;
    menuItem4.name=@"פניות שדווחו";
    menuItem4.color=self.Gray;
    menuItem4.highlightedColor=self.HighlightedGray;
    menuItem4.icon=[UIImage imageNamed:@"IconSend.png"];
    menuItem4.strColor=@"gray";
    
    MenuItem*menuItem5=[[MenuItem alloc]init];
    menuItem5.tag=5;
    menuItem5.name=@"דווח למוקד";
    menuItem5.color=self.Purple;
    menuItem5.highlightedColor=self.HighlightedPurple;
    menuItem5.icon=[UIImage imageNamed:@"IconAdd.png"];
    menuItem5.strColor=@"purple";
    
    MenuItem*menuItem6=[[MenuItem alloc]init];
    menuItem6.tag=6;
    menuItem6.name=@"מידע למשתמש";
    menuItem6.color=self.Orange;
    menuItem6.highlightedColor=self.HighlightedOrange;
    menuItem6.icon=[UIImage imageNamed:@"iconInformation"];
    menuItem6.strColor=@"orange";
    
    _menuItemsArray=[[NSArray alloc]initWithObjects:menuItem1,menuItem2,menuItem3,menuItem4,menuItem5,menuItem6, nil];
}

- (void)TouchDown:(UIButton*)sender
{
    sender.backgroundColor=((MenuItem*)[_menuItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tag =[cd] %d",((UIButton*) sender).tag]][0]).highlightedColor;
}
- (IBAction)ButtonClick:(id)sender
{
    SubViewController*view;
    UIButton*button=sender;
    MenuItem*menuItem=[_menuItemsArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"tag =[cd] %d",button.tag]][0];
    if(button.tag==6)
        view=[[InformationViewController alloc]init];
    else
    {
        if(button.tag==5)
            view=[[NewAppealViewController alloc]init];
        else
        {
            view=[[TableViewController alloc]init];
            ((TableViewController*)view).tag=menuItem.tag;
            
        }
    }
    view.color=menuItem.color;
    view.highlightedColor=menuItem.highlightedColor;
    view.strColor=menuItem.strColor;
    view.header=menuItem.name;
    view.strColor=menuItem.strColor;
    button.backgroundColor=menuItem.color;
    [self.navigationController pushViewController:view animated:YES];
}

-(void)SetControllers
{
    _btn1=[[UIButton alloc]init];
    _btn2=[[UIButton alloc]init];
    _btn3=[[UIButton alloc]init];
    _btn4=[[UIButton alloc]init];
    _btn5=[[UIButton alloc]init];
    _btn6=[[UIButton alloc]init];
    _buttonsArray=[[NSArray alloc]initWithObjects:_btn1,_btn2,_btn3,_btn4,_btn5,_btn6 ,nil];
    _imgNumbrer1=[[UIImageView alloc]init];
    _imgNumbrer2=[[UIImageView alloc]init];
    _imgNumbrer3=[[UIImageView alloc]init];
    _imgNumbrer4=[[UIImageView alloc]init];
    _imgNumbrer5=[[UIImageView alloc]init];
    _imgNumbrer6=[[UIImageView alloc]init];
    _imgsArray=[[NSArray alloc]initWithObjects:_imgNumbrer1,_imgNumbrer2,_imgNumbrer3,_imgNumbrer4,_imgNumbrer5,_imgNumbrer6,nil];
    _btn6=[[UIButton alloc]init];
    _lblsArray=[[NSArray alloc]initWithObjects:_lblNumbrer1,_lblNumbrer2,_lblNumbrer3,nil];
    for(int i=0;i<_buttonsArray.count;i++)
    {
        UIButton*button=_buttonsArray[i];
        MenuItem*menuItem=_menuItemsArray[i];
        if(menuItem.tag%2)
            button.frame=CGRectMake(XOrigion+widthSize+space, YOrigion+(heightSize+space)*(i/2), widthSize, heightSize);
        else
            button.frame=CGRectMake(XOrigion, YOrigion+(heightSize+space)*(i/2), widthSize, heightSize);
        [button setTitle:menuItem.name forState:normal];
        if(self.view.frame.size.height>480)
        {
            button.titleLabel.font=[UIFont fontWithName:@"Helvetica Bold" size:17];
            button.layer.cornerRadius=7;
            [button setTitleEdgeInsets:UIEdgeInsetsMake(64,-62, 0, 0)];
        }
        else
        {
            button.titleLabel.font=[UIFont fontWithName:@"Helvetica Bold" size:14];
            button.layer.cornerRadius=5;
            [button setTitleEdgeInsets:UIEdgeInsetsMake(50,-62, 0, 0)];
        }
        [button setBackgroundColor:menuItem.color];
        button.tag=menuItem.tag;
        button.titleLabel.textAlignment=NSTextAlignmentCenter;
        [button setImageEdgeInsets:UIEdgeInsetsMake(button.frame.size.width*15/90,button.frame.size.width*24/90,button.frame.size.width*35/90,button.frame.size.width*24/90)];
        [button setImage:menuItem.icon forState:normal];
        [button setImage:menuItem.icon forState:UIControlStateHighlighted];
        [button addTarget:self action:@selector(ButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [button addTarget:self action:@selector(TouchDown:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:button];
        if(i<3)
        {
            UIImageView*img=_imgsArray[i];
            UILabel*lbl=_lblsArray[i];
            img.frame=CGRectMake(button.frame.origin.x+63+button.frame.size.width-90, button.frame.origin.y-4, 30, 31);
            img.image=[UIImage imageNamed:[NSString stringWithFormat:@"%d",i+1]];
            lbl.frame=CGRectMake(button.frame.origin.x+65+button.frame.size.width-90, button.frame.origin.y-6, 30, 31);
            lbl.textAlignment=NSTextAlignmentCenter;
            lbl.font=[UIFont fontWithName:@"Helvetica" size:14];
            lbl.textColor=[UIColor whiteColor];
            [self.view addSubview:img];
            [self.view addSubview:lbl];
        }
    }
    
    _lblVersion=[[UILabel alloc]initWithFrame:CGRectMake(10, self.view.frame.size.height-32, self.width-20, 10)];
    NSString *versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    _lblVersion.text=[NSString stringWithFormat:@"%@ גרסה",versionString];
    _lblVersion.font=[UIFont fontWithName:@"Helvetica" size:11];
    _lblVersion.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:_lblVersion];
    
    _crmc=[[UILabel alloc]initWithFrame:CGRectMake((self.width-230)/2, self.view.frame.size.height-17, 230, 10)];
    _crmc.text=@"כל הזכויות שמורות ל-CRMC";
    _crmc.font=[UIFont fontWithName:@"Helvetica" size:11];
    _crmc.textAlignment=NSTextAlignmentRight;
    [self.view addSubview:_crmc];
    
    _btnLinkCrmc=[[UIButton alloc]initWithFrame:CGRectMake((self.width-230)/2, self.view.frame.size.height-17, 230, 10)];
    [_btnLinkCrmc setTitle:@"http://www.crmc.co.il" forState:normal];
    [_btnLinkCrmc setTitleColor:[UIColor blueColor] forState:normal];
    _btnLinkCrmc.titleLabel.font=[UIFont fontWithName:@"Helvetica" size:11];
    _btnLinkCrmc.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_btnLinkCrmc addTarget:self action:@selector(LinkCrmcClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnLinkCrmc];
}

- (IBAction)LinkCrmcClick:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:((UIButton*)sender).titleLabel.text]];
}
@end
