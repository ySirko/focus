//
//  AddAppealtreatmentViewController.m
//  crmcFocus
//
//  Created by Lior Ronen on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "AddAppealtreatmentViewController.h"
#import "AppealTableViewCell.h"

static NSString *ViewControllerName = @"AddAppealtreatment screen";

@interface AddAppealtreatmentViewController () <UIScrollViewDelegate, UITextViewDelegate, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate>
{
    int actionTypeId;
    UIAlertView *alertReport;
}

@property (nonatomic) BOOL isKeyboardShown;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIView *imagesContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@property (strong, nonatomic) UIPickerView *pickerViewStatus;

@property (strong, nonatomic) NSArray *listStatus;
@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) NSMutableArray *images;
@property (assign, nonatomic) NSInteger currentImage;

@end

@implementation AddAppealtreatmentViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = ViewControllerName;
    _isKeyboardShown = NO;
    
    [self registerNibs];
    [self prepareUI];
    [self getActionTypes];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self prepareDataSource];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self subscribeToNotification];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _listStatus.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return ((Status*)_listStatus[row]).Text;
}

#pragma mark - UITouch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.textField resignFirstResponder];
    [self.textView resignFirstResponder];
    
    if ([self.textView.text isEqualToString:@""]) {
        self.textView.text = @"תאור";
        self.textView.textColor = [UIColor lightGrayColor];
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.textView.textColor = [UIColor blackColor];
    if ([self.textView.text isEqualToString:@"תאור"]) {
        self.textView.text = @"";
    }
    return YES;
}

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        if ([self.textView.text isEqualToString: @""]) {
            self.textView.text=@"תאור";
            self.textView.textColor=[UIColor lightGrayColor];
        }
        [txtView resignFirstResponder];
    }
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppealTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AppealTableViewCell class])];
    if (indexPath.row < 2) {
        [cell.titleLabel setFont:[UIFont boldSystemFontOfSize:14.f]];
    }
    
    cell.titleLabel.text = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self heightFromString:self.dataSource[indexPath.row] labelWidth:CGRectGetWidth(self.tableView.bounds)];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *string = self.dataSource[indexPath.row];
    
    if ([string rangeOfString:self.appeal.ReporterCellPhoneNumber].location != NSNotFound) {
        [self makeCallFromNumber:self.appeal.ReporterCellPhoneNumber];
    }
    
    if ([string rangeOfString:self.appeal.ReporterHomePhoneNumber].location != NSNotFound) {
        [self makeCallFromNumber:self.appeal.ReporterHomePhoneNumber];
    }
}

#pragma mark - Keyboard Notifications

- (void)keyboardWillShow
{
    if (!_isKeyboardShown) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-170, self.view.frame.size.width, self.view.frame.size.height);
        _isKeyboardShown = !_isKeyboardShown;
    }
}

- (void)keyboardWillHide
{
    if (_isKeyboardShown) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+170, self.view.frame.size.width, self.view.frame.size.height);
        _isKeyboardShown = !_isKeyboardShown;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.view endEditing:YES];
    if ([alertView.message isEqualToString:@"שורת טיפול נוספה בהצלחה"]||[alertView.message isEqualToString:@"נסה שנית מאוחר יותר"]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [alertView dismissWithClickedButtonIndex:buttonIndex animated:YES];
    }
}

#pragma mark - UIImagePickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *newImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    if (!self.images) {
        self.images = [NSMutableArray new];
    }
    [self.images addObject:newImage];
    
    for (UIImageView *imageView in [self.imagesContainerView subviews]) {
        if (self.currentImage == imageView.tag) [imageView setImage:newImage];
    }
}

#pragma mark - IBActions

- (IBAction)selectImage:(id)sender
{
    UITapGestureRecognizer *recognizer = sender;
    self.currentImage = recognizer.view.tag;
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"בחר באפשרות הרצויה:" delegate:self cancelButtonTitle:@"סגירה" destructiveButtonTitle:nil otherButtonTitles:@"מצלמה",@"גלרית תמונות",nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    [self.textField resignFirstResponder];
    [self.textView resignFirstResponder];
}

- (IBAction)submitButtonPressed:(id)sender
{
    if (![self.textView.text isEqualToString:@""] && ![self.textView.text isEqualToString:@"תאור"] && ![self.textField.text isEqualToString:@""]) {
        
        NSArray *pictures = [NSArray arrayWithObjects:[self setImageGarbage:self.leftImageView.image], [self setImageGarbage:self.rightImageView.image], nil];
        
        ActionLine *actionLine = [[ActionLine alloc] initWith:_appeal.ID ActionTypeId:actionTypeId Text:self.textView.text Pictures:pictures];
        [self.generic showNativeActivityIndicator:self];
        [self.connection connectionToService:[NSString stringWithFormat:@"%@/AddActionLine",self.appDelegate.http] jsonDictionary:[actionLine parseActionLineToDict] controller:self withSelector:@selector(AddActionLineResult:)];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"תקלה" message:@"חלק משדות החובה חסרים או שתוכנם אינו חוקי. אנא מלאו ונסו שוב" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

#pragma mark - Private Methods

- (void)subscribeToNotification
{
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(lockScreen) name:@"lockedScreen" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide) name:UIKeyboardWillHideNotification object:nil];
}

- (CGFloat)heightFromString:(NSString *)string labelWidth:(CGFloat)width
{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14.f]};
    NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:attributes];
    CGRect expectedRectangle = [attributedString boundingRectWithSize:CGSizeMake(CGRectGetWidth(self.tableView.bounds), 2000.f) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    return CGRectGetHeight(expectedRectangle);
}

- (void)getActionTypes
{
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:self.appDelegate.city forKey:@"city"];
    [dic setObject:self.appDelegate.userName  forKey:@"userName"];
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:[NSString stringWithFormat:@"%@/GetActionTypes",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(GetActionTypesResult:)];
}

- (void)registerNibs
{
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([AppealTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([AppealTableViewCell class])];
}

- (void)prepareUI
{
    self.listStatus = [[NSArray alloc]init];
    
    self.pickerViewStatus = [[UIPickerView alloc] init];
    self.pickerViewStatus.showsSelectionIndicator=YES;
    self.pickerViewStatus.dataSource = self;
    self.pickerViewStatus.delegate = self;
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.width,44)];
    UIBarButtonItem *buttonOK = [[UIBarButtonItem alloc]initWithTitle:@"אישור" style:(UIBarButtonItemStyle) UIBarButtonItemStyleBordered target:self action:@selector(selectStatus:)];
    UIBarButtonItem *buttonCancel = [[UIBarButtonItem alloc]initWithTitle:@"ביטול" style:(UIBarButtonItemStyle) UIBarButtonItemStyleBordered target:self action:@selector(cancelSelecting:)];
    
    [toolbar setItems:@[buttonOK, buttonCancel]];
    self.textField.inputView = self.pickerViewStatus;
    self.textField.inputAccessoryView = toolbar;
    
    self.textField.placeholder = @"סטטוס פניה";
    [self.textField.layer setBorderColor:self.color.CGColor];
    [self.textField.layer setBorderWidth:1.f];
    [self.textField.layer setCornerRadius:5.f];
    
    self.textView.text = @"תאור";
    self.textView.textColor = [UIColor lightGrayColor];
    [self.textView.layer setBorderColor:self.color.CGColor];
    [self.textView.layer setBorderWidth:1.f];
    [self.textView.layer setCornerRadius:5.f];
    
    [self.submitButton setTitle:@"שלח" forState:UIControlStateNormal];
    [self.submitButton setBackgroundColor:self.color];
    
    [self.imagesContainerView.layer setBorderColor:self.color.CGColor];
    [self.imagesContainerView.layer setBorderWidth:1.f];
    [self.imagesContainerView.layer setCornerRadius:5.f];
    
    UITapGestureRecognizer *leftImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImage:)];
    leftImageTap.numberOfTapsRequired = 1;
    leftImageTap.numberOfTouchesRequired = 1;
    self.leftImageView.gestureRecognizers = @[leftImageTap];
    
    UITapGestureRecognizer *rightImageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectImage:)];
    rightImageTap.numberOfTapsRequired = 1;
    rightImageTap.numberOfTouchesRequired = 1;
    self.rightImageView.gestureRecognizers = @[rightImageTap];
}

- (void)prepareDataSource
{
    self.dataSource = [NSMutableArray new];
    
    [self.dataSource addObject:[NSString stringWithFormat:@"נושא: %@",_appeal.Subject]];
    [self.dataSource addObject:[NSString stringWithFormat:@"תאור: %@",_appeal.Descriptionn]];
    [self.dataSource addObject:[NSString stringWithFormat:@"כתובת: %@",_appeal.Address]];
    [self.dataSource addObject:[NSString stringWithFormat:@"שם הפונה: %@ %@",_appeal.ReporterFirstName,_appeal.ReporterLastName]];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm MM/dd/yyyy"];
    [self.dataSource addObject:[NSString stringWithFormat: @"תאריך פתיחה: %@",[dateFormatter stringFromDate:_appeal.OpenDate]]];
    [self.dataSource addObject:[NSString stringWithFormat:@"טלפון נייח: %@", _appeal.ReporterHomePhoneNumber]];
    [self.dataSource addObject:[NSString stringWithFormat:@"טלפון נייד: %@", _appeal.ReporterCellPhoneNumber]];
    [self.tableView reloadData];
    
    CGFloat tableHeight = 0;
    for (NSString *string in self.dataSource) {
        tableHeight += [self heightFromString:string labelWidth:CGRectGetWidth(self.tableView.bounds)];
    }
    
    [UIView animateWithDuration:0.25f animations:^{
        self.tableViewHeightConstraint.constant = tableHeight;
        [self.view layoutIfNeeded];
    }];
}

- (void)lockScreen
{
    [self.textView resignFirstResponder];
    [self.textField resignFirstResponder];
}

- (void)GetActionTypesResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if (!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound)) {
        Status*status=[[Status alloc]init];
        _listStatus=[status parseStatusListFromJson:result];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles:nil];
        [alert show];
    }
}

- (CGFloat)countLabelHeightWithString:(NSString *)string
{
    CGRect frame = [string boundingRectWithSize:CGSizeMake(275,320) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18.0f]} context:nil];
    float height = frame.size.height;
    return height;
}

- (void)makeCallFromNumber:(NSString *)phoneNumber
{
    if (!phoneNumber.length || !phoneNumber) {
        return;
    }

    NSString *theCall = [NSString stringWithFormat:@"tel://%@", phoneNumber];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:theCall]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theCall]];
    }
}

- (void)selectStatus:(id)sender
{
    int row = (int)[self.pickerViewStatus selectedRowInComponent:0];
    self.textField.text = ((Status*)[_listStatus objectAtIndex:row]).Text;
    actionTypeId = ((Status*)[_listStatus objectAtIndex:row]).Id;
    [self.textField resignFirstResponder];
}

- (void)cancelSelecting:(id)sender
{
    [self.textField resignFirstResponder];
}

- (NSString*)setImageGarbage:(UIImage*)image
{
    NSData *data = nil;
    NSString *imageData = nil;
    NSData *dataForJPEGFile;
    if(image==Nil)
        return @"";
    else
        dataForJPEGFile = UIImageJPEGRepresentation(image, 1.0);
    image=[[UIImage alloc]initWithData:dataForJPEGFile];
    CGSize newSize = CGSizeMake(image.size.width/3, image.size.height/3);
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    data=UIImagePNGRepresentation(newImage);
    dataForJPEGFile = UIImageJPEGRepresentation(newImage, 0.2);
    imageData = [dataForJPEGFile base64EncodedString];
    NSLog(@"image lenght: %lu",(unsigned long)[imageData length]);
    return imageData;
}

- (void)AddActionLineResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if (!([result rangeOfString:@"\"ResultCode\":1,"].location == NSNotFound)) {
        alertReport = [[UIAlertView alloc]initWithTitle:@"" message:@"שורת טיפול נוספה בהצלחה" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alertReport show];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            [self openCamera];
            break;
        case 1:
            [self openGallery];
            break;
        default:
            break;
    }
}

- (void)openCamera
{
    UIImagePickerController *image = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        image.sourceType = UIImagePickerControllerSourceTypeCamera;
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה" message:@"מצטערים, אין חיבור של מצלמה במכשיר זה" delegate:nil cancelButtonTitle:@"אשור" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    image.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:image.sourceType];
    NSArray *mediatypes = [NSArray arrayWithObject:image.mediaTypes.firstObject];
    image.mediaTypes = mediatypes;
    image.delegate = self;
    [self presentViewController:image animated:YES completion:nil ];
}

- (void)openGallery
{
    UIImagePickerController *image = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
        image.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"תקלה" message:@"מצטערים הגלריה אינה זמינה" delegate:nil cancelButtonTitle:@"אישור" otherButtonTitles:nil];
        [alert show];
        return;
    }
    image.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:image.sourceType];
    image.delegate = self;
    [self presentViewController:image animated:YES completion:nil ];
}

@end;
