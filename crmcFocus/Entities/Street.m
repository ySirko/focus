//
//  Street.m
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "Street.h"

@implementation Street
-(NSMutableArray*)parseStreetListFromJson:(NSString*) strJson
{
    NSError* error;
    NSDictionary* dic=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arr=[[NSMutableArray alloc]init];
    for (NSDictionary* dict in [dic objectForKey:@"Entities"])
        [arr addObject:[self parseStreetFromJson:dict]];
    return  arr;
}
-(Street*)parseStreetFromJson:(NSDictionary*)dic
{
    Street*street=[[Street alloc]init];
    street.StreetId=(int)[[dic objectForKey:@"StreetId"] integerValue];
    street.StreetName= [dic objectForKey:@"StreetName"];
    return street;
}
@end
