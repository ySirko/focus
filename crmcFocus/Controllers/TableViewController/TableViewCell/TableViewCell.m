//
//  TableViewCell.m
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "TableViewCell.h"
#import "ActionLine.h"
@implementation TableViewCell

//- (instancetype)initWithWidth:(NSInteger)width{
//    self = [super init];
//    if (self){
//        self.width = width;
//    }
//    return self;
//}

- (void)awakeFromNib {}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
-(void)setReportedInquiriesCell:(Appeal*)appeal
{
    
    
    _lblNumber=[[UILabel alloc]initWithFrame:CGRectMake(0, 10, self.width - 10, 15)];
    _lblNumber.text=[NSString stringWithFormat:@"%d",appeal.ID];
    _lblNumber.font=[UIFont fontWithName:@"Helvetica-Bold" size:18];
    _lblNumber.textAlignment=1;
    [self addSubview:_lblNumber];

    
    _lblDescription=[[UILabel alloc]initWithFrame:CGRectMake(0, 30,self.width-10, 15)];
    _lblDescription.text=[NSString stringWithFormat:@"%@",appeal.Descriptionn];
    _lblDescription.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblDescription.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblDescription];
    
    
    _lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(0, 50,self.width-10, 15)];
    _lblAddress.text=[NSString stringWithFormat:@"כתובת: %@",appeal.Address];

    _lblAddress.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblAddress.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblAddress];

    
    _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 70,self.width-10, 15)];
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm dd/MM/yyyy"];
    _lblDate.text = [NSString stringWithFormat:@"תאריך: %@",[df stringFromDate:appeal.OpenDate]];
    _lblDate.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblDate.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblDate];

//    _lblSendDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 90,self.width-10,15)];
//    NSDateFormatter* df1 = [[NSDateFormatter alloc]init];
//    [df1 setDateFormat:@"HH:mm dd/MM/yyyy"];
//    _lblSendDate.text = [NSString stringWithFormat:@"תאריך שליחה לפקח: %@", [df1 stringFromDate:appeal.SendDate]];
//    _lblSendDate.font=[UIFont fontWithName:@"Helvetica" size:13];
//    _lblSendDate.textAlignment = NSTextAlignmentRight;
//    [self addSubview:_lblSendDate];
    
    _lblApplicants=[[UILabel alloc]initWithFrame:CGRectMake(0, 110,self.width-10, 15)];
    _lblApplicants.text=[NSString stringWithFormat:@"כמות הפונים: %d",appeal.StatusId];
    _lblApplicants.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblApplicants.textAlignment = NSTextAlignmentRight;
    //[self addSubview:_lblApplicants];
    
    _btnDetails=[[UIButton alloc]initWithFrame:CGRectMake(14, 60, 17, 26)];
    [_btnDetails setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@Chetz",_strColor]] forState:UIControlStateNormal];
    
    [self addSubview:_btnDetails];
    _lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0,128,_width,4)];
    _lblLine.backgroundColor=_color;
    [self addSubview:_lblLine];

}

-(void)SetCell:(Appeal*)appeal
    {
    _lblNumber=[[UILabel alloc]initWithFrame:CGRectMake(0, 10,self.width,20)];
    _lblNumber.text=[NSString stringWithFormat:@"%d",appeal.ID];
    _lblNumber.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    _lblNumber.textAlignment=1;

    //[self addSubview:_lblNumber];

    
    _lblDescription=[[UILabel alloc]initWithFrame:CGRectMake(0, 30,self.width-10, 15)];
    _lblDescription.text=[NSString stringWithFormat:@"נושא:  %@",appeal.Subject];
    _lblDescription.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblDescription.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblDescription];
    
   
    
 
    
    _lblAddress=[[UILabel alloc]initWithFrame:CGRectMake(0, 50,self.width-10, 15)];
    if(appeal.Address.length>0)
        _lblAddress.text=[NSString stringWithFormat:@"כתובת: %@",[appeal.Address substringWithRange:NSMakeRange(0,[appeal.Address rangeOfString:@"הערות"].location)]];
        else
             _lblAddress.text=@"כתובת:";
    _lblAddress.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblAddress.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblAddress];
    
    
    _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(0, 70,self.width-10, 15)];
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm dd/MM/yyyy"];
    _lblDate.text = [NSString stringWithFormat:@"תאריך: %@",[df stringFromDate:appeal.OpenDate]];
    _lblDate.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblDate.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblDate];
    

    _lblApplicants=[[UILabel alloc]initWithFrame:CGRectMake(0, 90,self.width-10, 15)];
    _lblApplicants.text=[NSString stringWithFormat:@"כמות הפונים: %d",appeal.StatusId];
    _lblApplicants.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblApplicants.textAlignment = NSTextAlignmentRight;
     [self addSubview:_lblApplicants];
        
  
//    _lblSendDate=[[UILabel alloc]initWithFrame:CGRectMake(40, 90,_width-60,15)];
//    NSDateFormatter* df1 = [[NSDateFormatter alloc]init];
//    [df1 setDateFormat:@"HH:mm dd/MM/yyyy"];
//    _lblSendDate.text = [NSString stringWithFormat:@"תאריך שליחה לפקח: %@", [df1 stringFromDate:appeal.SendDate]];
//    _lblSendDate.font=[UIFont fontWithName:@"Helvetica" size:13];
//    _lblSendDate.textAlignment = NSTextAlignmentRight;
//     [self addSubview:_lblSendDate];
//    
//    _lblApplicants=[[UILabel alloc]initWithFrame:CGRectMake(0, 110,self.width-10, 20)];
//    _lblApplicants.text=[NSString stringWithFormat:@"כמות הפונים: %d",appeal.StatusId];
//    _lblApplicants.font=[UIFont fontWithName:@"Helvetica" size:13];
//    _lblApplicants.textAlignment = NSTextAlignmentRight;
//     [self addSubview:_lblApplicants];
    
    _lblName=[[UILabel alloc]initWithFrame:CGRectMake(40, 40,_width-60,20)];
    _lblName.text=[NSString stringWithFormat:@"שם הפונה: %@ %@",appeal.ReporterFirstName,appeal.ReporterLastName];
    _lblName.font=[UIFont fontWithName:@"Helvetica" size:13];
    _lblName.textAlignment = NSTextAlignmentRight;
     [self addSubview:_lblName];
    
   
    
    
    _btnDetails=[[UIButton alloc]initWithFrame:CGRectMake(14, 60, 17, 26)];
    [_btnDetails setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@Chetz",_strColor]] forState:UIControlStateNormal];
    
    [self addSubview:_btnDetails];
    
    _lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0,130,_width,4)];
    _lblLine.backgroundColor=_color;
    [self addSubview:_lblLine];

    if(![_lblSendDate.text isEqualToString:@""])
        _lblSendDate.frame=CGRectMake( 0, 90,self.width-10, 20);
    else
        _lblSendDate.hidden=YES;
    _lblName.hidden=YES;
    [self addSubview:_lblNumber];
}

-(void)SetActionLineCell:(ActionLine*)actionLine
{
    _lblDate=[[UILabel alloc]initWithFrame:CGRectMake(40, 2, _width-60, 20)];
    NSDateFormatter* df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"HH:mm dd/MM/yyyy"];
    _lblDate.text =[NSString stringWithFormat:@"תאריך %@", [df stringFromDate:actionLine.OpenDate]];
    _lblDate.font=[UIFont fontWithName:@"Helvetica" size:11];
    _lblDate.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblDate];

    _lblStatus=[[UILabel alloc]initWithFrame:CGRectMake(40, 27,_width-60, 20)];
    _lblStatus.text=[NSString stringWithFormat:@"סטטוס פניה  %@",actionLine.StatusName];
    _lblStatus.font=[UIFont fontWithName:@"Helvetica" size:11];
    _lblStatus.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblStatus];
    
    _lblDescription=[[UILabel alloc]initWithFrame:CGRectMake(40, 50, _width-60, 20)];
    _lblDescription.text=[NSString stringWithFormat:@"%@",actionLine.Text];
    _lblDescription.font=[UIFont fontWithName:@"Helvetica-Bold" size:13];
    _lblDescription.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblDescription];
    
    _lblName=[[UILabel alloc]initWithFrame:CGRectMake(40, 75,_width-60, 20)];
    _lblName.text=[NSString stringWithFormat:@"שם המפעיל:  %@",actionLine.EmployerName];
    _lblName.font=[UIFont fontWithName:@"Helvetica" size:11];
    _lblName.textAlignment = NSTextAlignmentRight;
    [self addSubview:_lblName];

    _lblLine=[[UILabel alloc]initWithFrame:CGRectMake(0, 100,_width, 4)];
    _lblLine.backgroundColor=_color;
    [self addSubview:_lblLine];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if(highlighted)
    {
        _btnDetails.imageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@ChetzHighlighted.png",_strColor]];
        _lblLine.backgroundColor=_highlightedColor;
    }
    else
    {
        _btnDetails.imageView.image=[UIImage imageNamed:[NSString stringWithFormat:@"%@Chetz.png",_strColor]];
        _lblLine.backgroundColor=_color;
    }
}

@end
