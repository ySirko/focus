//
//  InformationViewController.m
//  Focus
//
//  Created by MyMac on 11/19/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "InformationViewController.h"
#import "WebViewController.h"
@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _lblVarsion=[[UILabel alloc]initWithFrame:CGRectMake(20, 180, self.width-40, 20)];
    NSString *versionString = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    _lblVarsion.text=[NSString stringWithFormat:@"%@ מס׳ גרסה",versionString];
    _lblVarsion.textAlignment=NSTextAlignmentRight;
    _lblVarsion.font = [UIFont fontWithName:@"Helvetica" size:15];
    [self.view addSubview:_lblVarsion];
    
    _lblService=[[UILabel alloc]initWithFrame:CGRectMake(20, 210, self.width-40, 20)];
    _lblService.text=@"פותח ע״י חברת סי.אר.אם שיא בע״מ";
    _lblService.textAlignment=NSTextAlignmentRight;
    _lblService.font = [UIFont fontWithName:@"Helvetica" size:15];
    [self.view addSubview:_lblService];
    
    _btnLinkCrmc =[[UIHyperlinksButton alloc]init];
    _btnLinkCrmc.layer.frame= CGRectMake(45 , self.view.frame.size.height-220, 116, 30);
    [_btnLinkCrmc.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [_btnLinkCrmc setTitle:@"www.crmc.co.il" forState:UIControlStateNormal];
    [_btnLinkCrmc addTarget:self action:@selector(btnLinkClick:) forControlEvents:UIControlEventTouchUpInside];
    [_btnLinkCrmc setTitleColor:[UIColor blueColor] forState:normal];
    [_btnLinkCrmc setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [self.view addSubview:_btnLinkCrmc];
    
    _btnLinkSupport =[[UIHyperlinksButton alloc]init];
    _btnLinkSupport.layer.frame= CGRectMake(45 , self.view.frame.size.height-190, 145, 30);
    [_btnLinkSupport.titleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:13.0]];
    [_btnLinkSupport setTitle:@"support@crmc.co.il" forState:UIControlStateNormal];
    [_btnLinkSupport addTarget:self action:@selector(btnEmailClick:) forControlEvents:UIControlEventTouchUpInside];
    [_btnLinkSupport setTitleColor:[UIColor blueColor] forState:normal];
    [_btnLinkSupport setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [self.view addSubview:_btnLinkSupport];
    
    int widthImage=100;
    _imgView=[[UIImageView alloc]initWithFrame:CGRectMake(((self.view.frame.size.width)/2)-(widthImage/2),_btnLinkSupport.frame.origin.y+_btnLinkSupport.frame.size.height+15,widthImage,widthImage)];
    _imgView.backgroundColor=[UIColor clearColor];
    _imgView.layer.cornerRadius=4;
    _imgView.image=[UIImage imageNamed:@"152"];
    [self.view addSubview:_imgView];
}

- (IBAction)btnLinkClick:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.crmc.co.il"]];
}

- (IBAction)btnEmailClick:(id)sender
{
    NSString *url = [@"mailto:support@crmc.co.il?subject=&body=" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
}

@end
