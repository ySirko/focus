//
//  AppealTableViewCell.h
//  פוקוס בסלולר
//
//  Created by Yuriy Sirko on 10/21/15.
//  Copyright © 2015 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppealTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
