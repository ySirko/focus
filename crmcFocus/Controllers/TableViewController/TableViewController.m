//
//  TableViewController.m
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "TableViewController.h"
#import "TableViewCell.h"
#import "Appeal.h"
#import "DetailsViewController.h"
#import "SoundMenuViewController.h"

#define REGEX_FOR_NUMBERS   @"^([+-]?)(?:|0|[0-9]\\d*)(?:\\.\\d*)?$"
#define REGEX_FOR_INTEGERS  @"^([+-]?)(?:|0|[1-9]\\d*)?$"
#define IS_A_NUMBER(string) [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_FOR_NUMBERS] evaluateWithObject:string]
#define IS_AN_INTEGER(string) [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", REGEX_FOR_INTEGERS] evaluateWithObject:string]

@interface TableViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIBarPositioningDelegate>
{
    NSMutableArray* filteredCouncilOfficalArray;
}

@property (strong, nonatomic) UILabel *noResults;

@end

@implementation TableViewController

#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.searchBar.delegate = self;
    [self SetControllers];
    _searchBar.barStyle = UIBarStyleBlackTranslucent;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.view endEditing:YES];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.noResults = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-60, self.view.frame.size.height/2, 140, 20)];
    self.noResults.text = @"No results found.";
    [self.view addSubview:self.noResults];
    [self.noResults setHidden:YES];
    
    if (self.tag == 3) {
        self.isHideNumberOfTickets = NO;
    }
    
    self.screenName=@"appeals";
    NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
    [dic setObject:self.appDelegate.city forKey:@"city"];
    [dic setObject:self.appDelegate.userName  forKey:@"userName"];
    [dic setObject:[NSNumber numberWithInt:_tag] forKey:@"ticketTypes"];
    [self.generic showNativeActivityIndicator:self];
    [self.connection connectionToService:[NSString stringWithFormat:@"%@/GetTicketsList",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(GetTicketsListResult:)];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (_tag) {
        case 4:
            return 130;
            break;
        case 1:
        case 2:
        case 3:
            return 135;
            break;
            
        default:
            return 94;
            
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (filteredCouncilOfficalArray.count == 0) {
        filteredCouncilOfficalArray = [[NSMutableArray alloc] initWithArray:_listAppeals];
    }
    Appeal*a=filteredCouncilOfficalArray[indexPath.row];
    int i=a.ID;
    DetailsViewController *view=[[DetailsViewController alloc]init];
    view.appeal=filteredCouncilOfficalArray[indexPath.row];
    view.color=self.color;
    view.highlightedColor=self.highlightedColor;
    view.strColor=self.strColor;
    view.header=[NSString stringWithFormat:@"פרטי פניה: %d",i];
    
    view.tag=_tag;
    [self.navigationController pushViewController:view animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int ticketQuantity = [[[NSUserDefaults standardUserDefaults] objectForKey:@"ticketsQuantity"] intValue];
    
    if (![self.searchBar.text isEqualToString:@""]){
        if (filteredCouncilOfficalArray.count == 0) {
            [self.noResults setHidden:NO];
        } else {
            [self.noResults setHidden:YES];
        }
        return [filteredCouncilOfficalArray count];
    }
    else    {
        [self.noResults setHidden:YES];
        if (ticketQuantity) {
            if ([self.listAppeals count] > ticketQuantity){
                return [[[NSUserDefaults standardUserDefaults] objectForKey:@"ticketsQuantity"] intValue];
            } else {
                return [self.listAppeals count];
            }
        } else {
            return [self.listAppeals count];
        }
    }
}

- (TableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    cell.tag=_tag;
    cell.color=self.color;
    cell.highlightedColor=self.highlightedColor;
    cell.strColor=self.strColor;
    cell.tag=_tag;
    cell.width=self.width;
    
    
    
    Appeal *appeal = [[Appeal alloc]init];
    if (![self.searchBar.text isEqualToString:@""])
        appeal = [filteredCouncilOfficalArray objectAtIndex:indexPath.row];
    else
        appeal = [_listAppeals objectAtIndex:indexPath.row];
    if (appeal.AlreadyRead == NO) {
        cell.backgroundColor = [UIColor colorWithRed:237.f/255.f green:237.f/255.f blue:237.f/255.f alpha:248.f/255.f];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if(_tag == 4)
        [cell setReportedInquiriesCell:appeal];
    else
        [cell SetCell:appeal];
    return cell;
}

#pragma mark - Privte Methods

-(void)SetControllers
{
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 140, self.view.bounds.size.width, 44)];
    _searchBar.delegate = self;
    _searchBar.barTintColor=self.color;
    if(_tag!=4)
    {
        [self.view addSubview:_searchBar];
        _tblAppeals=[[UITableView alloc]initWithFrame:CGRectMake(0,self.searchBar.frame.origin.y+self.searchBar.frame.size.height,self.view.frame.size.width, self.view.frame.size.height-182)];
    }
    else
        _tblAppeals=[[UITableView alloc]initWithFrame:CGRectMake(0,self.lbl.frame.size.height+self.lbl.frame.origin.y,self.view.frame.size.width, self.view.frame.size.height-132)];
    
    _tblAppeals.delegate=self;
    _tblAppeals.dataSource=self;
    _tblAppeals.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tblAppeals];
    [self.view sendSubviewToBack:_tblAppeals];
    [self.view sendSubviewToBack:self.background];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{

    [self performSearchForSearchBar:searchBar];
    [searchBar resignFirstResponder];


}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{

    [self performSearchForSearchBar:searchBar];

}

- (void)performSearchForSearchBar:(UISearchBar*)searchBar
{
    filteredCouncilOfficalArray = [NSMutableArray new];
    
    NSString *s=_searchBar.text;
    NSString *sid;
    
    if(IS_A_NUMBER(s))
    {
        for (Appeal *a in _listAppeals)
        {
            sid = [NSString stringWithFormat:@"%d",a.ID];
            if ([sid rangeOfString:s].location != NSNotFound) {
                [filteredCouncilOfficalArray addObject:a];
            }
        }
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Address contains[c] %@",s];
        filteredCouncilOfficalArray = [NSMutableArray arrayWithArray:[_listAppeals filteredArrayUsingPredicate:predicate]];
    }

    [_tblAppeals reloadData];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.searchBar resignFirstResponder];
}

-(void)GetTicketsListResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if(!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound)) {
        _listAppeals=[Appeal parseAppealListFromJson:result];
        filteredCouncilOfficalArray = [[NSMutableArray alloc] initWithArray:_listAppeals];
        [_tblAppeals reloadData];
    } else {
        UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alert show];
    }
}

@end
