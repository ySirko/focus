//
//  DetailsViewController.h
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "SubViewController.h"
#import "Appeal.h"
#import "AddAppealtreatmentViewController.h"
@interface DetailsViewController : SubViewController<UIScrollViewDelegate>
@property int tag;
@property int appealID;
@property(retain,nonatomic)Appeal*appeal;
@property(retain,nonatomic)UILabel*lblDescriptioDetails;

//@property(retain,nonatomic)UILabel*lblCategory;
//@property(retain,nonatomic)UILabel*lblDescription;
//@property(retain,nonatomic)UILabel*lblDate;
//@property(retain,nonatomic)UILabel*lblName;
//@property(retain,nonatomic)UILabel*lblAddress;
//@property(retain,nonatomic)UILabel*lblNumber;
//@property(retain,nonatomic)UILabel*lblStandard;
//@property(retain,nonatomic)UIButton*lblPhone;
//@property(retain,nonatomic)UILabel*lblpho;
//@property(retain,nonatomic)UILabel*lblmob;
//@property(retain,nonatomic)UIButton*lblMobile;
@property(retain,nonatomic)UIButton*btnCareAppeal;
//@property(retain,nonatomic)UILabel*lblExpectedTimeToTreat;
@property(retain,nonatomic)UIImageView*imgApeal;
@property(retain,nonatomic)UILabel*lblLine;
@property(retain,nonatomic)UIButton*btnWaze;
//@property(retain,nonatomic)UILabel*lblComments;
@property (retain,nonatomic)NSString*notification;
@property(retain,nonatomic)UIScrollView*scrlImag;
@property(retain,nonatomic)UIPageControl*pgcontrol;
@property (nonatomic) int pushType;

@end
