//
//  ActionLine.m
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "ActionLine.h"
#import "AppDelegate.h"

@implementation ActionLine

- (id)initWith:(int)ticketId ActionTypeId:(int)actionTypeId Text:(NSString*)text Pictures:(NSArray *)pictures
{
    _TicketId=ticketId;
    _ActionTypeId=actionTypeId;
    _Text=text;
    _Pictures = pictures;
    return self;
}

- (NSMutableArray *)parseActionLineListFromJson:(NSString *)strJson
{
    NSError* error;
    NSDictionary* dic=[NSJSONSerialization JSONObjectWithData:[strJson dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&error];
    NSMutableArray* arr=[[NSMutableArray alloc]init];
    for (NSDictionary* dict in [dic objectForKey:@"Entities"])
        [arr addObject:[self parseActionLineFromJson:dict]];
    
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"OpenDate" ascending:NO];
    NSArray* sortedArray=[arr sortedArrayUsingDescriptors:@[sort]];
    arr=[[NSMutableArray alloc]initWithArray:sortedArray];
    return  arr;
}

- (ActionLine *)parseActionLineFromJson:(NSDictionary *)dic
{
    ActionLine*actionLine=[[ActionLine alloc]init];
    actionLine.TicketId=(int)[[dic objectForKey:@"TicketId"] integerValue];
    actionLine.ActionLineId=(int)[[dic objectForKey:@"ActionLineId"] integerValue];
    actionLine.StatusName= [dic objectForKey:@"StatusName"];
    actionLine.EmployerName= [dic objectForKey:@"EmployerName"];
    actionLine.Text= [dic objectForKey:@"Text"];
    actionLine.Pictures= [dic objectForKey:@"Pictures"];
    actionLine.OpenDate= [self getDateFromJSON:[dic objectForKey:@"OpenDate"]];
    return actionLine;
}

- (NSDictionary*)parseActionLineToDict
{
    AppDelegate*appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSMutableDictionary* dictt=[[NSMutableDictionary alloc]init];
    [dictt setObject:appDelegate.city forKey:@"city"];
    [dictt setObject:appDelegate.userName  forKey:@"userName"];
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:[NSNumber numberWithInt: _TicketId] forKey:@"TicketId"];
    [dict setObject:[NSNumber numberWithInt: _ActionTypeId] forKey:@"ActionTypeId"];
    [dict setObject:_Text forKey:@"Text"];
    
    NSMutableArray *picturesArray = [NSMutableArray new];
    for (NSString *img in _Pictures) {
        NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];
        [dic setObject:img forKey:@"GraphicBytesBase64"];
        [picturesArray addObject:dic];
    }
    
    [dict setObject:picturesArray forKey:@"Pictures"];
    [dictt setObject:dict forKey:@"actionLine"];
    return dictt;
}

- (NSDate*)getDateFromJSON:(NSString *)dateString
{
    // Expect date in this format "/Date(1268123281843)/"
    int startPos = (int)[dateString rangeOfString:@"("].location+1;
    int endPos = (int)[dateString rangeOfString:@")"].location;
    NSRange range = NSMakeRange(startPos,endPos-startPos);
    unsigned long long milliseconds = [[dateString substringWithRange:range] longLongValue];
    //NSLog(@"%llu",milliseconds);
    NSTimeInterval interval = milliseconds/1000;
    return [NSDate dateWithTimeIntervalSince1970:interval];
}

@end
