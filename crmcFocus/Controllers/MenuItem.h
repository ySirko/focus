//
//  MenuItem.h
//  crmcFocus
//
//  Created by MyMac on 10/28/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewController.h"

@interface MenuItem : NSObject

@property int tag;
@property (retain,nonatomic) NSString *name;
@property (retain,nonatomic) UIImage *icon;
@property (retain,nonatomic) UIColor *color;
@property (retain,nonatomic) UIColor *highlightedColor;
@property (retain,nonatomic) NSString *strColor;

@end
