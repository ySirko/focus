//
//  AppealTreatmentsViewController.h
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubViewController.h"
#import "Appeal.h"
@interface AppealTreatmentsViewController : SubViewController

@property(retain,nonatomic)NSArray*listAppeals;
@property(retain,nonatomic)UITableView*tblAppeals;
@property(retain,nonatomic)UIButton*btnAddLine;
@property(retain,nonatomic)UILabel*lblExsitsLine;
@property(retain,nonatomic)Appeal*appeal;
@property(retain,nonatomic)UILabel*lblLine;
@property (retain,nonatomic)NSString*notification;

@end
