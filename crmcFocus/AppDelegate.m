//
//  AppDelegate.m
//  crmcFocus
//
//  Created by Lior Ronen on 10/27/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "MenuViewController.h"
#import "NewAppealViewController.h"
#import "AddAppealtreatmentViewController.h"
#import "Generic.h"
#import "Connection.h"
#import "Notification.h"
#import "DetailsViewController.h"
#import "AppealTreatmentsViewController.h"
#import "Logger.h"
#import "ActionLine.h"
#include <AudioToolbox/AudioToolbox.h>

@import AVFoundation;

@interface AppDelegate ()
{
    Connection*connection;
    SubViewController *viewController;
    Logger *logger;
    UINavigationController *navigationController;
}

@property (strong, nonatomic) Notification *notification;
@property (strong, nonatomic) NSDictionary *notificationDictionary;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    navigationController = [[UINavigationController alloc] init];
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-64497015-1"];
    
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    tracker.allowIDFACollection = YES;
    _http=[[NSString alloc]init];
    
    if([[NSUserDefaults standardUserDefaults]objectForKey:@"userName"]==nil)
    {
        viewController =[[LoginViewController alloc]init];
        viewController.color=[UIColor colorWithRed:76.0f/255.0f green:0.0f/255.0f blue:121.0f/255.0f alpha:1.0f];
        viewController.header=@"כניסת משתמש";
        navigationController=[[UINavigationController alloc]initWithRootViewController:viewController];
        [_window setRootViewController:navigationController];
    }
    else
    {
        _city=[[NSUserDefaults standardUserDefaults]objectForKey:@"city"];
        _password=[[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
        _userName=[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
        NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
        [dic setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"city"] forKey:@"City"];
        [dic setObject:@"" forKey:@"ClientVersion"];
        [dic setObject:@"2.0.6" forKey:@"ClientIOSVersion"];
        connection=[[Connection alloc]init];
        [connection connectionToServiceCity:@"/GetServiceUrl" jsonDictionary:dic controller:self withSelector:@selector(GetServiceUrlResult:)];
    }
    
    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        NSLog(@"iOS 8 Requesting permission for push notifications..."); // iOS 8
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                                UIUserNotificationTypeAlert | UIUserNotificationTypeBadge |
                                                UIUserNotificationTypeSound categories:nil];
        [UIApplication.sharedApplication registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        NSLog(@"iOS 7 Registering device for push notifications..."); // iOS 7 and earlier
        [UIApplication.sharedApplication registerForRemoteNotificationTypes:
         UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |
         UIRemoteNotificationTypeSound];
    }

    if (launchOptions != nil) {
        self.notificationDictionary = [[NSDictionary alloc] init];
        self.notificationDictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    }
    return YES;
}

-(void)GetServiceUrlResult:(NSString*)result{
    result = [[[result description]stringByReplacingOccurrencesOfString: @"\\/" withString: @"/"]stringByReplacingOccurrencesOfString: @"\"" withString: @""] ;
    if(![result isEqualToString:@"UrlNotFound"])
    {
        _http=result;
        MenuViewController *menuMiewController =[[MenuViewController alloc]init];
        if (self.notificationDictionary.count > 0) {
            menuMiewController.notificationDictionary = self.notificationDictionary;
        }
        ((MenuViewController*)menuMiewController).color=[UIColor lightGrayColor];
        navigationController=[[UINavigationController alloc]initWithRootViewController:menuMiewController];
        [_window setRootViewController:navigationController];
    }
    else
        NSLog(@"Not found url");
}

- (void) application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    logger=[[Logger alloc]init];
    logger.FunctionName=@"didRegisterForRemoteNotificationsWithDeviceToken";
    
    _dt=[[NSString alloc]init];
    _dt = [[[[deviceToken description]
             stringByReplacingOccurrencesOfString: @"<" withString: @""]
            stringByReplacingOccurrencesOfString: @">" withString: @""]
           stringByReplacingOccurrencesOfString: @" " withString: @""] ;
    logger.Json=_dt;
    
    connection=[[Connection alloc]init];
    [connection connectionToLoggerService:@"/WriteLog" jsonDictionary:[logger parseLoggerToDict] controller:self withSelector:@selector(WriteLogResult:)];
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    BOOL isLogined = [[NSUserDefaults standardUserDefaults] boolForKey:@"isLogined"];
    if (!isLogined) {
        return;
    }
    self.notification = [[Notification alloc]init];
    self.notification.ticketId=(int)[[userInfo objectForKey:@"TicketId"]integerValue];
    self.notification.title=[userInfo objectForKey:@"Title"];
    self.notification.pushType=(int)[[userInfo objectForKey:@"PushType"]integerValue];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Push notification received" message:@" האם ברצונך לעבור לפניה?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes",nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == [alertView cancelButtonIndex]) {
        [alertView dismissWithClickedButtonIndex:[alertView cancelButtonIndex] animated:YES];
    } else {
        [self switchPushType];
    }
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings{
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)notification completionHandler:(void(^)())completionHandler
{
    completionHandler();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self.window endEditing:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    application.applicationIconBadgeNumber = 0;
}

-(void)WriteLogResult:(NSString*)result
{
    NSLog(@"WriteLogResult");
}

- (void)switchPushType
{
    switch (self.notification.pushType)
    {
        case 1:
        {
            [self pushDetailsViewController];
            break;
        }
        case 2:
        {
            [self pushApealTreatmentsViewController];
            break;
        }
        case 3:
        {
            [self pushDetailsViewController];
            break;
        }
        default:
            break;
    }
}

- (void)pushDetailsViewController
{
    DetailsViewController *view=[[DetailsViewController alloc]init];
    view.appeal=[[Appeal alloc]init];
    view.appeal.ID = self.notification.ticketId;
    view.color=[UIColor colorWithRed:255.0f/255.0f green:142.0f/255.0f blue:12.0f/255.0f alpha:1.0f];
    view.highlightedColor=[UIColor colorWithRed:246.0f/255.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0f];
    view.strColor=@"orange";
    view.header=@"פרטי פנייה";
    view.tag=2;
    view.pushType = self.notification.pushType;
    view.notification=self.notification.msg;
    navigationController=[[UINavigationController alloc]initWithRootViewController:view];
    [_window setRootViewController:navigationController];
}

- (void)pushApealTreatmentsViewController
{
    AppealTreatmentsViewController *view=[[AppealTreatmentsViewController alloc]init];
    view.appeal=[[Appeal alloc]init];
    view.appeal.ID=self.notification.ticketId;
    view.color=[UIColor colorWithRed:255.0f/255.0f green:142.0f/255.0f blue:12.0f/255.0f alpha:1.0f];
    view.highlightedColor=[UIColor colorWithRed:246.0f/255.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0f];
    view.strColor=@"orange";
    view.header=@"טיפול בפניה";
    view.notification=self.notification.msg;
    navigationController=[[UINavigationController alloc]initWithRootViewController:view];
    [_window setRootViewController:navigationController];
}

- (void)applicationWillResignActive:(UIApplication *)application{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"lockedScreen" object:nil];
}

@end
