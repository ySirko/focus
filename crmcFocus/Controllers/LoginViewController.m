//
//  LoginViewController.m
//  crmcFocus
//
//  Created by Lior Ronen on 10/27/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "LoginViewController.h"
#import "MenuViewController.h"
#import "Logger.h"
#import "Connection.h"
@interface LoginViewController () <UITextFieldDelegate>
{
    UIAlertView *alertView;
    int num;
    int YOrigion;
    Logger *logger;
    Connection*connection;
}

@property (nonatomic) BOOL isKeyboardShown;

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isKeyboardShown = NO;
    self.screenName = @"login Screen";
    Logger *loggerInstance=[[Logger alloc]init];
    loggerInstance.FunctionName=@"viewDidLoad";
    loggerInstance.Json=@"odeya";
    Connection *connectionInstance=[[Connection alloc]init];
    [connectionInstance connectionToLoggerService:@"" jsonDictionary:[logger parseLoggerToDict] controller:self withSelector:@selector(WriteLogResult:)];
    
    if(self.view.frame.size.height>480)
        YOrigion=185;
    else
        YOrigion=150;
    
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userName"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"password"];
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"city"];
    [self setDynamicObject];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector:@selector(lockScreen)
                                                 name:@"lockedScreen"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)lockScreen
{
    [_txtUserName resignFirstResponder];
    [_txtPassword resignFirstResponder];
    [_txtOrganizationCode resignFirstResponder];
}

-(void)setDynamicObject
{
    self.lbl.backgroundColor=self.color;
    
    self.lblbackgroung.backgroundColor=self.color;
    
    _txtUserName=[[UITextField alloc]initWithFrame:CGRectMake(10,35+YOrigion,self.width-20,45)];
    _txtUserName.font = [UIFont fontWithName:@"ChalkboardSE" size:13];
    _txtUserName.delegate=self;
    _txtUserName.textAlignment=NSTextAlignmentRight;
    _txtUserName.backgroundColor = [UIColor whiteColor];
    _txtUserName.textColor = [UIColor blackColor];
    _txtUserName.layer.borderColor=self.color.CGColor;
    _txtUserName.layer.borderWidth=0.5;
    _txtUserName.layer.cornerRadius=4;
    _txtUserName.borderStyle = UITextBorderStyleRoundedRect;
    _txtUserName.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtUserName.returnKeyType = UIReturnKeyDone;
    _txtUserName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _txtUserName.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtUserName.placeholder=@"שם משתמש:";
    _txtUserName.tag=1;
    
    _txtUserName.keyboardType = UIKeyboardTypeNumberPad;
    
    [self.view addSubview:_txtUserName];
    
    _txtPassword=[[UITextField alloc]initWithFrame:CGRectMake(10,85+YOrigion,self.width-20,45)];
    _txtPassword.font = [UIFont fontWithName:@"ChalkboardSE" size:15];
    _txtPassword.delegate=self;
    _txtPassword.textAlignment=NSTextAlignmentRight;
    _txtPassword.backgroundColor = [UIColor whiteColor];
    _txtPassword.textColor = [UIColor blackColor];
    _txtPassword.layer.borderColor=self.color.CGColor;
    _txtPassword.layer.borderWidth=0.5;
    _txtPassword.layer.cornerRadius=4;
    _txtPassword.borderStyle = UITextBorderStyleRoundedRect;
    _txtPassword.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtPassword.returnKeyType = UIReturnKeyDone;
    _txtPassword.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _txtPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtPassword.secureTextEntry = YES;
    _txtPassword.placeholder=@"סיסמא:";
    _txtPassword.tag=2;
    
    _txtPassword.keyboardType = UIKeyboardTypeNumberPad;
    
    [self.view addSubview:_txtPassword];
    
    _txtOrganizationCode=[[UITextField alloc]initWithFrame:CGRectMake(10,138+YOrigion,self.width-20,45)];
    _txtOrganizationCode.font = [UIFont fontWithName:@"ChalkboardSE" size:13];
    _txtOrganizationCode.delegate=self;
    _txtOrganizationCode.textAlignment=NSTextAlignmentRight;
    _txtOrganizationCode.backgroundColor = [UIColor whiteColor];
    _txtOrganizationCode.textColor = [UIColor blackColor];
    _txtOrganizationCode.layer.borderColor=self.color.CGColor;
    _txtOrganizationCode.layer.borderWidth=0.5;
    _txtOrganizationCode.layer.cornerRadius=4;
    _txtOrganizationCode.borderStyle = UITextBorderStyleRoundedRect;
    _txtOrganizationCode.clearButtonMode = UITextFieldViewModeWhileEditing;
    _txtOrganizationCode.returnKeyType = UIReturnKeyDone;
    _txtOrganizationCode.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _txtOrganizationCode.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _txtOrganizationCode.secureTextEntry = YES;
    _txtOrganizationCode.placeholder=@"קוד ארגון:";
    _txtOrganizationCode.tag=3;
    [self.view addSubview:_txtOrganizationCode];
    
    _btnIsConnected =[[UIButton alloc]init];
    _btnIsConnected.layer.frame= CGRectMake(10,195+YOrigion,self.width-20,40);
    _btnIsConnected.layer.cornerRadius=4;
    [_btnIsConnected setTitle:@"השאר מחובר במכשיר זה" forState:UIControlStateNormal];
    [_btnIsConnected addTarget:self action:@selector(btnIsConnectedClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnIsConnected.backgroundColor=[UIColor grayColor ];
    [self.view addSubview:_btnIsConnected];
    
    _btnIsEnter =[[UIButton alloc]initWithFrame:CGRectMake(5,270+YOrigion,self.width-10,45)];
    _btnIsEnter.layer.cornerRadius=3;
    _btnIsEnter.titleLabel.font=[UIFont boldSystemFontOfSize:20.0f];
    [_btnIsEnter setTitle:@"הכנס" forState:UIControlStateNormal];
    [_btnIsEnter addTarget:self action:@selector(btnIsEnterClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnIsEnter.backgroundColor=self.color;
    [self.view addSubview:_btnIsEnter];
    
    _imgConnect=[[UIImageView alloc]initWithFrame:CGRectMake(self.width-50,200+YOrigion,28,28)];
    _imgConnect.backgroundColor=[UIColor whiteColor];
    _imgConnect.layer.cornerRadius=4;
    [self.view addSubview:_imgConnect];
    
    if(self.view.frame.size.height>480)
    {
        _txtUserName.frame=CGRectMake(10, 40+YOrigion, self.width-20, 50);
        _txtPassword.frame=CGRectMake(10,100+YOrigion, self.width-20, 50);
        _txtOrganizationCode.frame=CGRectMake(10,160+YOrigion, self.width-20, 50);
        _btnIsConnected.layer.frame= CGRectMake(10 , 230+YOrigion, self.width-20, 50);
        _imgConnect.layer.frame=CGRectMake(self.width-58, 238+YOrigion, 34, 34);
        _btnIsEnter.frame=CGRectMake(5, 330+YOrigion, self.width-10, 46);
    }
}
- (IBAction)btnIsConnectedClick:(id)sender
{
    if(_IsConnected)
    {
        _IsConnected=NO;
        _imgConnect.image=[UIImage imageNamed:@"x.png"];
    }
    else
    {
        _IsConnected=YES;
        _imgConnect.image=[UIImage imageNamed:@"v.png"];
    }
}

- (IBAction)btnIsEnterClick:(id)sender
{
    if (![_txtUserName.text isEqualToString:@""]&&![_txtPassword.text isEqualToString:@""]&&![_txtOrganizationCode.text isEqualToString:@""])
    {
        NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
        [dic setObject:_txtOrganizationCode.text forKey:@"City"];
        [dic setObject:@"" forKey:@"ClientVersion"];
        [dic setObject:@"2.0.6" forKey:@"ClientIOSVersion"];
        NSString *pushSound = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushNotificationSound"];
        if (pushSound){
            [dic setObject:pushSound forKey:@"sound"];
        }else {
            NSString *defSound = @"default";
            [dic setObject:defSound forKey:@"sound"];
        }
        if(self.appDelegate.dt)
        {
            [dic setObject:self.appDelegate.dt forKey:@"phoneNumber"];
        }
        else {
            [dic setObject:@"" forKey:@"phoneNumber"];
        }
        [self.generic showNativeActivityIndicator:self];
        [self.connection connectionToServiceCity:@"/GetServiceUrl" jsonDictionary:dic controller:self withSelector:@selector(getServiceUrlResult:)];
    }
    else
    {
        alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"יש למלא שדות חובה" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alertView show];
    }
}

-(void)getServiceUrlResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    result = [[[result description]stringByReplacingOccurrencesOfString: @"\\/" withString: @"/"]stringByReplacingOccurrencesOfString: @"\"" withString: @""] ;
    if(![result isEqualToString:@"UrlNotFound"]){
        self.appDelegate.http=result;
        NSMutableDictionary*dic=[[NSMutableDictionary alloc]init];
        [dic setObject:_txtOrganizationCode.text forKey:@"city"];
        [dic setObject:_txtUserName.text forKey:@"username"];
        [dic setObject:[NSString stringWithFormat:@"%@#0",_txtPassword.text] forKey:@"password"];
        NSString *pushSound = [[NSUserDefaults standardUserDefaults] objectForKey:@"pushNotificationSound"];
        if (pushSound){
            [dic setObject:pushSound forKey:@"sound"];
        }else {
            NSString *defSound = @"default";
            [dic setObject:defSound forKey:@"sound"];
        }
        if(self.appDelegate.dt){
            Logger *loggerInstance=[[Logger alloc]init];
            loggerInstance.FunctionName=@"getServiceUrlResult";
            loggerInstance.Json=@"udid is nil";
            [dic setObject:self.appDelegate.dt forKey:@"phoneNumber"];
            Connection *connectionInstance=[[Connection alloc]init];
            [connectionInstance connectionToLoggerService:@"/WriteLog" jsonDictionary:[logger parseLoggerToDict] controller:self withSelector:@selector(WriteLogResult:)];
        }
        else {
            [dic setObject:@"" forKey:@"phoneNumber"];
        }
        [self.generic showNativeActivityIndicator:self];
        [self.connection connectionToService:[NSString stringWithFormat:@"%@/Login",self.appDelegate.http] jsonDictionary:dic controller:self withSelector:@selector(LoginResult:)];
    }
    else
    {
        alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"קוד ארגון אינו קיים" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alertView show];
    }
}

-(void)WriteLogResult:(NSString*)result
{
    NSLog(@"WriteLogResult");
}

-(void)LoginResult:(NSString*)result
{
    [self.generic hideNativeActivityIndicator:self];
    if([result rangeOfString:@"UserNotFound"].location==NSNotFound&&!([result rangeOfString:@"Entities"].location==NSNotFound)&&!([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound))
    {
        if(_IsConnected)
        {
            [[NSUserDefaults standardUserDefaults]setObject:_txtUserName.text forKey:@"userName"];
            [[NSUserDefaults standardUserDefaults]setObject:_txtPassword.text forKey:@"password"];
            [[NSUserDefaults standardUserDefaults]setObject:_txtOrganizationCode.text forKey:@"city"];
        }
        self.appDelegate.userName=_txtUserName.text;
        self.appDelegate.password=_txtPassword.text;
        self.appDelegate.city=_txtOrganizationCode.text;
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogined"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        alertView=[[UIAlertView alloc]initWithTitle:@"" message:@"פרטיך נקלטו בהצלחה" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
        [alertView show];
    }
    else
    {
        if(!([result rangeOfString:@"UserNotFound"].location==NSNotFound))
        {
            UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"" message:@"אחד הפריטים שהוקשו שגויים" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
            [alert show];
        }
        else if([result rangeOfString:@"\"ResultCode\":1,"].location==NSNotFound)
        {
            UIAlertView*alert=[[UIAlertView alloc]initWithTitle:@"תקלה" message:@"נסה שנית מאוחר יותר" delegate:self cancelButtonTitle:@"אישור" otherButtonTitles: nil];
            [alert show];
        }
    }
}

- (void)alertView:(UIAlertView *)alert clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alert.message isEqualToString:@"פרטיך נקלטו בהצלחה"])
    {
        MenuViewController* view=[[MenuViewController alloc]init];
        view.color=[UIColor lightGrayColor];
        [self.navigationController pushViewController:view animated:NO];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self lockScreen];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void)keyboardWillShow {
    if (!_isKeyboardShown) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-160, self.view.frame.size.width, self.view.frame.size.height);
        _isKeyboardShown = !_isKeyboardShown;
    }
}

-(void)keyboardWillHide {
    if (_isKeyboardShown) {
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+160, self.view.frame.size.width, self.view.frame.size.height);
        _isKeyboardShown = !_isKeyboardShown;
    }
}

@end
