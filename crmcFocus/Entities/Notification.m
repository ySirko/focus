//
//  Notification.m
//  crmcFocus
//
//  Created by MyMac on 11/12/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "Notification.h"

@implementation Notification

-(Notification*)parseNotificationListFromJson:(NSDictionary*) dic
{

    
    Notification*notification=[[Notification alloc]init];
    notification.ticketId=(int)[[dic objectForKey:@"TicketId"]integerValue];
    notification.title=[dic objectForKey:@"Title"];
    notification.pushType=(int)[[dic objectForKey:@"PushType"]integerValue];
    return notification;
}
@end
