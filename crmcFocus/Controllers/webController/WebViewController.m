//
//  WebViewController.m
//  NewCrmc
//
//  Created by MyMac on 7/31/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@end

@implementation WebViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.web=[[UIWebView alloc]initWithFrame:CGRectMake(0, 132, 320, self.view.frame.size.height-140)];
    self.web.scalesPageToFit = YES;
    [self.view addSubview:self.web];
    if(_IsHtml)
        [self.web loadHTMLString:self.nsurl baseURL:nil];
    else
    {
        NSURL*url=[NSURL URLWithString:self.nsurl];
        NSURLRequest*urlrr=[NSURLRequest requestWithURL:url];
        [self.web loadRequest:urlrr];
    }
}

@end
