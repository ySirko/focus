//
//  SoundMenuViewController.m
//  
//
//  Created by Dmitry Kanivets on 11.06.15.
//
//

@import AVFoundation;

#import "SoundMenuViewController.h"
#import "soundMenuCellTableViewCell.h"
#import "AppDelegate.h"

@interface SoundMenuViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSMutableArray *sounds;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIView *dividerLine;
@property (weak, nonatomic) IBOutlet UILabel *labelHeader;
@property (weak, nonatomic) IBOutlet UIButton *cancelBut;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) AVAudioPlayer *audioplayer;
@property (strong, nonatomic) NSString *choosedSound;
@property (weak, nonatomic) IBOutlet UIButton *soundButton;
@property (weak, nonatomic) IBOutlet UIButton *ticketsButton;
@property (strong, nonatomic) NSMutableArray *tickets;
@property (nonatomic) BOOL isTickets;
@property (weak, nonatomic) IBOutlet UIImageView *ticketImage;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (assign ,nonatomic) NSInteger ticketsQuantity;

@property (copy, nonatomic) NSString *chossedSoundFromuserDefaults;
//@property (assign, nonatomic) NSInteger ticketQuantityFromuserDefaults;

@end

@implementation SoundMenuViewController

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:211/255 green:211/255 blue:211/255 alpha:0.6];
    self.sounds = [[NSMutableArray alloc] init];
    self.tickets = [[NSMutableArray alloc] initWithObjects:@(20),@(50),@(100), nil];
  
//  self.ticketQuantityFromuserDefaults = [[[NSUserDefaults standardUserDefaults] valueForKey:@"ticketsQuantity"] intValue];
    NSString *musicString = [[NSUserDefaults standardUserDefaults] valueForKey:@"pushNotificationSound"];
    if (musicString) {
    NSArray *temp = [musicString componentsSeparatedByString:@"."];
    self.chossedSoundFromuserDefaults = [temp firstObject];
    }
    //gavnokod
    [self.container bringSubviewToFront:self.ticketImage];
    [self hideItems];
    [self addSoundItems];
}

#pragma mark - Custom Accessors

- (void)setIsHideNumberOfTickets:(BOOL)isHideNumberOfTickets
{
    _isHideNumberOfTickets = isHideNumberOfTickets;
}

#pragma mark Private methods

- (void)addSoundItems
{
    NSString *music1 = @"Alarm clock going off.wav";
    NSString *music2 = @"Archery-arrow fly by.wav";
    NSString *music3 = @"Bite.wav";
    NSString *music4 = @"Bugle.wav";
    NSString *music5 = @"Cartoon bing sound.wav";
    NSString *music6 = @"Camera snapping picture.wav";
    NSString *music7 = @"Canary song.wav";
    NSString *music8 = @"Car horn.wav";
    NSString *music9 = @"Cartoon sound.wav";
    NSString *music10 = @"Church bell.wav";
    NSString *music11 = @"Comical drip sound.wav";
    NSString *music12 = @"Computers talking.wav";
    NSString *music13 = @"Door slaming.wav";
    NSString *music14 = @"Doorbell.wav";
    NSString *music15 = @"Finger cymbals.wav";
    NSString *music16 = @"Glass breaking.wav";
    NSString *music17 = @"Long trill.wav";
    NSString *music18 = @"Photon.wav";
    NSString *music19 = @"Single bird song.wav";
    NSString *music20 = @"Space entity.wav";
    NSString *music21 = @"Space launch.wav";
    NSString *music22 = @"Space light beam.wav";
    NSString *music23 = @"Symphony symbols.wav";
    NSString *music24 = @"Toughtone phone.wav";
    NSString *music25 = @"Water dripping.wav";
    NSString *music26 = @"Pew-pew.caf";
    NSString *music27 = @"default";
    
    [self.sounds addObject:music1];
    [self.sounds addObject:music2];
    [self.sounds addObject:music3];
    [self.sounds addObject:music4];
    [self.sounds addObject:music5];
    [self.sounds addObject:music6];
    [self.sounds addObject:music7];
    [self.sounds addObject:music8];
    [self.sounds addObject:music9];
    [self.sounds addObject:music10];
    [self.sounds addObject:music11];
    [self.sounds addObject:music12];
    [self.sounds addObject:music13];
    [self.sounds addObject:music14];
    [self.sounds addObject:music15];
    [self.sounds addObject:music16];
    [self.sounds addObject:music17];
    [self.sounds addObject:music18];
    [self.sounds addObject:music19];
    [self.sounds addObject:music20];
    [self.sounds addObject:music21];
    [self.sounds addObject:music22];
    [self.sounds addObject:music23];
    [self.sounds addObject:music24];
    [self.sounds addObject:music25];
    [self.sounds addObject:music26];
    [self.sounds addObject:music27];
    self.isTickets = NO;
}

- (void)hideItems
{
    [self.tableView setHidden:YES];
    [self.labelHeader setHidden:YES];
    [self.okButton setHidden:YES];
    [self.cancelBut setHidden:YES];
    [self.dividerLine setHidden:YES];

    [self.ticketsButton setHidden:self.isHideNumberOfTickets];
    [self.ticketImage setHidden:self.isHideNumberOfTickets];
}

- (void)unhideItems
{
    [self.tableView setHidden:NO];
    [self.labelHeader setHidden:NO];
    [self.okButton setHidden:NO];
    [self.cancelBut setHidden:NO];
    [self.dividerLine setHidden:NO];
}

#pragma mark IBAction methods

- (IBAction)cancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)okbut:(id)sender
{
    if (!_isTickets) {
        [[NSUserDefaults standardUserDefaults] setObject:self.choosedSound forKey:@"pushNotificationSound"];
    } else {
        [[NSUserDefaults standardUserDefaults] setInteger:self.ticketsQuantity forKey:@"ticketsQuantity"];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)dismissPopUp:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openTableView:(id)sender
{
    _isTickets = NO;
    if (sender == self.soundButton) {
        [self unhideItems];
        [self.dismissButton setHidden:YES];
    }
    
}

- (IBAction)openTicketsNumberPicker:(id)sender {
    _isTickets = YES;
    if (sender == self.ticketsButton) {
        [self unhideItems];
        [self.dismissButton setHidden:YES];
        [self.tableView reloadData];
    }
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (_isTickets) {
        return self.tickets.count;
    } else
    return self.sounds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[soundMenuCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        [[NSBundle mainBundle] loadNibNamed:@"soundMenuCellTableViewCell" owner:self options:nil];
        cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
        cell.textLabel.textAlignment = 2;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue Bold" size:12];
        cell.textLabel.textColor = [UIColor colorWithRed:20/255 green:115/255 blue:108/255 alpha:1];
    }
    [cell.textLabel setTextColor:[UIColor colorWithRed:20/255 green:115/255 blue:108/255 alpha:1]];

    cell.imageView.image = [UIImage imageNamed:@"on"];
    cell.imageView.highlightedImage = [UIImage imageNamed:@"off"];
    cell.imageView.contentMode = UIViewContentModeScaleAspectFit;
    cell.textLabel.textAlignment = 2;
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica Neue Bold" size:12];
    cell.textLabel.textColor = [UIColor colorWithRed:20/255 green:115/255 blue:108/255 alpha:1];
    
    if (_isTickets) {
        cell.textLabel.text = [NSString stringWithFormat:@"%@",self.tickets[indexPath.row]];
        NSInteger currentCellNumber = [self.tickets[indexPath.row] intValue];
        NSInteger numberOfChoosenItems = [[[NSUserDefaults standardUserDefaults] valueForKey:@"ticketsQuantity"] intValue];
      if (numberOfChoosenItems == currentCellNumber) {
          self.ticketsQuantity = currentCellNumber;
        [self selectCellAtIndexPath:indexPath];
      } else {
          NSLog(@"hui %lu -- %lu", (long)numberOfChoosenItems, (long)currentCellNumber);
      }
    } else {
    cell.textLabel.text = [self.sounds[indexPath.row] stringByDeletingPathExtension];
  }
    
  if ([cell.textLabel.text isEqualToString:self.chossedSoundFromuserDefaults]) {
      self.choosedSound = self.sounds[indexPath.row];
      
    [self selectCellAtIndexPath:indexPath];
  }
    
    return cell;
}

- (void)selectCellAtIndexPath:(NSIndexPath *)indexPath {
  [self.tableView selectRowAtIndexPath:indexPath
                         animated:NO
                   scrollPosition:UITableViewScrollPositionNone];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_isTickets) {
        self.ticketsQuantity = [self.tickets[indexPath.row] intValue];
    } else {
    NSString *string = self.sounds[indexPath.row];
    NSString* fileName = [string stringByDeletingPathExtension];
    if (![fileName isEqualToString:@"default"]) {
        NSString* extension = [string pathExtension];
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:fileName ofType:extension];
        NSError *error;
        NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
        self.audioplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundURL error:&error];
        [self.audioplayer prepareToPlay];
        [self.audioplayer play];
    }
    self.choosedSound = string;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell resignFirstResponder];
}


@end
