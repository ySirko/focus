//
//  InformationViewController.h
//  Focus
//
//  Created by MyMac on 11/19/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import "SubViewController.h"
#import "UIHyperlinksButton.h"
@interface InformationViewController : SubViewController
@property(retain,nonatomic)UILabel*lblVarsion;
@property(retain,nonatomic)UILabel*lblService;
@property(retain,nonatomic )UIButton*btnLinkCrmc;
@property(retain,nonatomic )UIButton*btnLinkSupport;
@property(retain,nonatomic )UIImageView*imgView;
@end
