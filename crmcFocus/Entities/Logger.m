//
//  Logger.m
//  מגדל העמק
//
//  Created by MyMac on 11/5/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import "Logger.h"

@implementation Logger
-(NSDictionary*)parseLoggerToDict
{
    NSMutableDictionary* dict=[[NSMutableDictionary alloc]init];
    [dict setObject:@"focus" forKey:@"ProjectName"];
    [dict setObject:_FunctionName forKey:@"FunctionName"];
    [dict setObject:_Json forKey:@"Json"];
    [dict setObject:@"webit" forKey:@"ToUTL"];
    // NSDictionary* dictReport=[NSDictionary dictionaryWithObjectsAndKeys:dict,@"AttachedFiles", nil];
    return dict;
}
@end
