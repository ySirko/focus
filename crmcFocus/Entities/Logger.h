//
//  Logger.h
//  מגדל העמק
//
//  Created by MyMac on 11/5/14.
//  Copyright (c) 2014 webit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Logger : NSObject
@property(retain,nonatomic)NSString*ProjectName;
@property(retain,nonatomic)NSString*FunctionName;
@property(retain,nonatomic)NSString*Json;
@property(retain,nonatomic)NSString*ToUTL;
-(NSDictionary*)parseLoggerToDict;
@end
