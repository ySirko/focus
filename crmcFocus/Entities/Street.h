//
//  Street.h
//  crmcFocus
//
//  Created by MyMac on 10/30/14.
//  Copyright (c) 2014 racheliKrimalovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Street : NSObject
@property int StreetId;
@property (retain,nonatomic)NSString* StreetName;
-(NSMutableArray*)parseStreetListFromJson:(NSString*) strJson;
@end
