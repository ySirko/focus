//
//  imagePreviewViewController.m
//  פוקוס בסלולר
//
//  Created by Dmitry Kanivets on 11.08.15.
//  Copyright (c) 2015 racheliKrimalovski. All rights reserved.
//

#import "imagePreviewViewController.h"

@interface imagePreviewViewController () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *retakeImageButton;

@end

@implementation imagePreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageView.image = self.imagePreview;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissVC)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
  if (self.isActiveretakeButton) {
    self.retakeImageButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.retakeImageButton.layer.borderWidth = 1.0;
    self.retakeImageButton.layer.cornerRadius = 5.0;
  } else {
    [self.retakeImageButton setHidden:YES];
  }

}

- (void)dismissVC{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setImagePreview:(UIImage *)imagePreview {
  _imagePreview = imagePreview;
  self.imageView.image = _imagePreview;
}

#pragma mark - IBAction

- (IBAction)retakeImageButtonAction:(id)sender {
  if (self.retackePicktureAction) {
    [self dismissVC];
    self.retackePicktureAction();
  }
}
@end
