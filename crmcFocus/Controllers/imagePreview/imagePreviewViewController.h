//
//  imagePreviewViewController.h
//  פוקוס בסלולר
//
//  Created by Dmitry Kanivets on 11.08.15.
//  Copyright (c) 2015 racheliKrimalovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imagePreviewViewController : UIViewController

@property (strong, nonatomic) UIImage *imagePreview;

@property (nonatomic, copy) void (^retackePicktureAction)(void);
@property (assign, nonatomic) BOOL isActiveretakeButton;

@end
